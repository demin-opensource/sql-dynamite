﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using SqlDynamite.Common;

namespace SqlDynamite
{
    /// <summary>
    /// Interaction logic for DriverWindow.xaml
    /// </summary>
    public partial class DriverWindow
    {
        public DriverWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var items = new ObservableCollection<Tuple<string, string>>();
            GridView view = new GridView();
            view.Columns.Add(new GridViewColumn { Header = "DB", DisplayMemberBinding = new Binding("Item1") });
            view.Columns.Add(new GridViewColumn { Header = "Provider", DisplayMemberBinding = new Binding("Item2") });
            foreach (Tuple<string, string> provider in MetadataFinderHelper.ProvidersList.Distinct())
            {
                items.Add(provider);
            }
            Providers.View = view;
            Providers.ItemsSource = items;
        }
    }
}
