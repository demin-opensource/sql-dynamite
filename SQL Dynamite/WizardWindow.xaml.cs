﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using SqlDynamite.Common;
using SqlDynamite.Util;

namespace SqlDynamite
{
    /// <summary>
    /// Interaction logic for WizardWindow.xaml
    /// </summary>
    public partial class WizardWindow
    {
        private class TableEntry
        {
            public string col_name { get; set; }
            public string col_typename { get; set; }
            public string col_len { get; set; }
            public string col_prec { get; set; }
            public string col_scale { get; set; }
        }

        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _cancellationToken;

        public WizardWindow()
        {
            InitializeComponent();
        }

        private void SrcComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var serverInfo = XmlUtil.SelectConfig(ListWindow.ConfigFile, SrcComboBox.SelectedItem.ToString());
            ListObjects(GenerateSelectScript(serverInfo.User, serverInfo.ServerType), SrcComboBox, SrcListView);
        }

        private void DstComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SrcComboBox.SelectedIndex > -1)
            {
                var serverInfo = XmlUtil.SelectConfig(ListWindow.ConfigFile, DstComboBox.SelectedItem.ToString());
                ListObjects(GenerateSelectScript(serverInfo.User, serverInfo.ServerType), DstComboBox, DstListView);
                var srcListViewItems = SrcListView.Items.Cast<ListViewItem>().ToArray();
                foreach (ListViewItem listViewItem in DstListView.Items)
                {
                    var srcListViewItem = srcListViewItems.FirstOrDefault(t => t.Content.Equals(listViewItem.Content));
                    if (srcListViewItem != null)
                    {
                        listViewItem.Background = new SolidColorBrush(Colors.LightCyan);
                        srcListViewItem.Background = new SolidColorBrush(Colors.LightCyan);
                    }
                    else
                    {
                        listViewItem.Background = new SolidColorBrush(Colors.Transparent);
                    }
                }
            }
            else if (DstComboBox.SelectedIndex > -1)
            {
                DstComboBox.SelectedIndex = -1;
                MessageBox.Show("Please select source DB first.");
            }
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            if (SrcComboBox.SelectedIndex < 0 || DstComboBox.SelectedIndex < 0)
            {
                return;
            }
            var tables = (from srcItem in SrcListView.Items.Cast<ListViewItem>()
                join dstItem in DstListView.Items.Cast<ListViewItem>() on srcItem.Content equals dstItem.Content
                select srcItem.Content.ToString()).ToArray();
            btnRun.IsEnabled = false;
            btnStop.IsEnabled = true;
            string errors = null;
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
            string srcDb = SrcComboBox.SelectedItem.ToString();
            string dstDb = DstComboBox.SelectedItem.ToString();
            Task.Factory.StartNew(() => errors = CompareTables(tables, srcDb, dstDb, _cancellationToken), _cancellationToken).ContinueWith(task => ShowLogWindow(errors), TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            _cancellationTokenSource.Cancel();
            Progress.Value = 0;
            btnRun.IsEnabled = true;
            btnStop.IsEnabled = false;
        }

        private void ShowLogWindow(string log)
        {
            var logWindow = new LogWindow {ShowInTaskbar = false, Owner = this};
            logWindow.LogTextBox.Document.Blocks.Clear();
            logWindow.LogTextBox.Document.Blocks.Add(new Paragraph(new Run(log)));
            logWindow.ShowDialog();
            Progress.Value = 0;
            btnRun.IsEnabled = true;
            btnStop.IsEnabled = false;
        }

        private string CompareTables(string[] tables, string srcDb, string dstDb, CancellationToken cancellationToken)
        {
            IList<string> errors = new List<string>();
            for (int index = 0; index < tables.Length; index++)
            {
                try
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    string error = CompareTable(tables[index], srcDb, dstDb);
                    if (string.IsNullOrWhiteSpace(error))
                    {
                    }
                    else
                    {
                        errors.Add($"TABLE {tables[index]}:{Environment.NewLine}{error}{Environment.NewLine}******************************************");
                    }
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch (Exception exc)
                {
                    _cancellationTokenSource.Cancel();
                    MessageBox.Show(exc.GetExceptionMessages());
                    break;
                }
                var percentage = 100*index/tables.Length;
                Dispatcher.Invoke(delegate { Progress.Value = percentage; });
            }
            return string.Join(Environment.NewLine, errors);
        }

        private string CompareTable(string tableName, string srcDb, string dstDb)
        {
            string[] floatTypes = { "NUMBER", "numeric", "decimal" };
            string[] stringTypes = { "char", "nchar", "varchar", "nvarchar", "VARCHAR2" };

            var srcServerInfo = XmlUtil.SelectConfig(ListWindow.ConfigFile, srcDb);
            string srcScript = GenerateDefScript(tableName, srcServerInfo.ServerType);
            var srcTableDef = GetTableDefinition<TableEntry>(srcScript, srcServerInfo);
            var dstServerInfo = XmlUtil.SelectConfig(ListWindow.ConfigFile, dstDb);
            string dstScript = GenerateDefScript(tableName, dstServerInfo.ServerType);
            var dstTableDef = GetTableDefinition<TableEntry>(dstScript, dstServerInfo);
            string errors = string.Empty;
            foreach (var srcColumn in srcTableDef)
            {
                var dstColumn = dstTableDef.FirstOrDefault(t => t.col_name.ToLower() == srcColumn.col_name.ToLower());
                if (dstColumn == null)
                {
                    errors += $"Column {srcColumn.col_name} is missing.{Environment.NewLine}";
                }
                else
                {
                    if (srcServerInfo.ServerType == dstServerInfo.ServerType)
                    {
                        if (srcColumn.col_typename != dstColumn.col_typename)
                        {
                            errors += $"Column {srcColumn.col_name} has different data type ({srcColumn.col_typename} and {dstColumn.col_typename}).{Environment.NewLine}";
                        }
                    }
                    if (stringTypes.Contains(srcColumn.col_typename) && stringTypes.Contains(dstColumn.col_typename))
                    {
                        if (srcColumn.col_len != dstColumn.col_len)
                        {
                            errors += $"Column {srcColumn.col_name} has different length ({srcColumn.col_len} and {dstColumn.col_len}).{Environment.NewLine}";
                        }
                    }
                    else if (floatTypes.Contains(srcColumn.col_typename) && floatTypes.Contains(dstColumn.col_typename))
                    {
                        if (srcColumn.col_prec != null && dstColumn.col_prec != null && srcColumn.col_prec != dstColumn.col_prec)
                        {
                            errors += $"Column {srcColumn.col_name} has different precision ({srcColumn.col_prec} and {dstColumn.col_prec}).{Environment.NewLine}";
                        }
                        if (srcColumn.col_scale != null && dstColumn.col_scale != null && srcColumn.col_scale != dstColumn.col_scale)
                        {
                            errors += $"Column {srcColumn.col_name} has different scale ({srcColumn.col_scale} and {dstColumn.col_scale}).{Environment.NewLine}";
                        }
                    }
                }
            }
            return errors;
        }

        private static IList<T> GetTableDefinition<T>(string script, ServerInfo serverInfo) where T : new()
        {
            var list = new List<T>();
            MetadataFinder finder = ConfigWindow.CreateMetadataFinder(serverInfo.ServerType, false);
            try
            {
                var connectionType = serverInfo.ConnectionType == "OLEDB" ? ConnectionType.OLEDB : serverInfo.ConnectionType == "ODBC" ? ConnectionType.ODBC : ConnectionType.NET;
                string cstring = finder.MakeConnectionString(serverInfo.Driver, serverInfo.Server, serverInfo.Database, serverInfo.User,
                    serverInfo.Password, serverInfo.SSPI == "True", connectionType);
                finder.EstablishConnection(cstring, connectionType, serverInfo.Database);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.GetExceptionMessages(), "Error while establishing connection", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            var rows = finder.CreateDataSet(script).Tables[0].Rows;
            foreach (DataRow dataRow in rows)
            {
                var obj = new T();
                foreach (DataColumn dataColumn in dataRow.Table.Columns)
                {
                    string columnName = dataColumn.ColumnName.ToLower();
                    PropertyInfo prop = obj.GetType().GetProperty(columnName, BindingFlags.Public | BindingFlags.Instance);
                    object val = dataRow[dataColumn.ColumnName.ToLower()];
                    if (prop != null && prop.CanWrite && !(val is DBNull))
                    {
                        prop.SetValue(obj, val.ToString(), null);
                    }
                }
                list.Add(obj);
            }
            return list;
        }

        private static void ListObjects(string query, ComboBox comboBox, ListView listView)
        {
            var serverInfo = XmlUtil.SelectConfig(ListWindow.ConfigFile, comboBox.SelectedItem.ToString());
            MetadataFinder finder = ConfigWindow.CreateMetadataFinder(serverInfo.ServerType, false);
            try
            {
                var connectionType = serverInfo.ConnectionType == "OLEDB" ? ConnectionType.OLEDB : serverInfo.ConnectionType == "ODBC" ? ConnectionType.ODBC : ConnectionType.NET;
                string cstring = finder.MakeConnectionString(serverInfo.Driver, serverInfo.Server, serverInfo.Database, serverInfo.User,
                    serverInfo.Password, serverInfo.SSPI == "True", connectionType);
                finder.EstablishConnection(cstring, connectionType, serverInfo.Database);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.GetExceptionMessages(), "Error while establishing connection", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var tables = finder.GetObjects(query);
            listView.Items.Clear();
            foreach (var table in tables)
            {
                listView.Items.Add(new ListViewItem { Content = table.Item1 });
            }
        }

        private string GenerateSelectScript(string userId, string serverType)
        {
            string script = null;
            if (serverType == "Oracle")
            {
                script = $"SELECT OBJECT_NAME, OBJECT_TYPE FROM ALL_OBJECTS WHERE OBJECT_TYPE = 'TABLE' AND LOWER(OWNER) = LOWER('{userId}') ORDER BY OBJECT_NAME";
            }
            else if (serverType == "MSSQL")
            {
                script = "select name, xtype from sysobjects where xtype = 'U' order by name";
            }
            else if (serverType == "SAP ASE")
            {
                script = "select name, type from sysobjects where type = 'U' order by name";
            }
            else if (serverType == "SAP Anywhere" || serverType == "SAP IQ")
            {
                script = "select name, type from sysobjects where type = 'U' and substring(name, 1, 3) != 'ml_' and substring(name, 1, 3) != 'sa_' and substring(name, 1, 4) != 'spt_' and substring(name, 1, 12) != 'synchronize_' order by name";
            }
            return script;
        }

        private string GenerateDefScript(string tableName, string serverType)
        {
            string script = null;
            if (serverType == "Oracle")
            {
                script = "SELECT COLUMN_NAME col_name, DATA_TYPE col_typename, DATA_LENGTH col_len, DATA_PRECISION col_prec, DATA_SCALE col_scale FROM ALL_TAB_COLUMNS WHERE LOWER(TABLE_NAME) = LOWER('" + tableName + "')";
            }
            else if (serverType == "MSSQL")
            {
                script = $"select c.name col_name, t.name col_typename, length col_len, xprec col_prec, xscale col_scale from syscolumns c inner join sys.types t on c.xusertype = t.user_type_id inner join sysobjects o on c.id = o.id where o.name = '{tableName}'";
            }
            else if (serverType == "SAP ASE")
            {
                script = $"select c.name col_name, t.name col_typename, c.length col_len, c.prec col_prec, c.scale col_scale from syscolumns c inner join systypes t on c.usertype = t.usertype inner join sysobjects o on c.id = o.id where o.name = '{tableName}'";
            }
            else if (serverType == "SAP Anywhere" || serverType == "SAP IQ")
            {
                script = $"select c.column_name col_name, t.domain_name col_typename, c.width col_len, c.width col_prec, c.scale col_scale from systabcol c inner join sysdomain t on c.domain_id = t.domain_id inner join sysobjects o on c.table_id = o.id where o.name = '{tableName}'";
            }
            return script;
        }
    }
}
