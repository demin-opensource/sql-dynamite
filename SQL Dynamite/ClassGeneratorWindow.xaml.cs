﻿using System;
using System.Windows;
using System.Windows.Input;
using SqlDynamite.Common;
using SqlDynamite.Util;

namespace SqlDynamite
{
    /// <summary>
    /// Interaction logic for ClassGenerator.xaml
    /// </summary>
    public partial class ClassGeneratorWindow
    {
        public MetadataFinder MetadataFinder { private get; set; }

        public ClassGeneratorWindow()
        {
            InitializeComponent();
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            GenerateCode();
        }

        private void txtScript_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                GenerateCode();
            }
        }

        private void GenerateCode()
        {
            try
            {
                ConfigWindow.SyntaxProvider.LoadLanguage(LanguageComboBox.Text);
                ConfigWindow.SyntaxProvider.InitializeSyntaxHighlighter(box1, true);
                var table = MetadataFinder.CreateDataSet(ScriptTextBox.Text).Tables[0];
                string str = ClassGenerator.GenerateClass(LanguageComboBox.Text,
                    AnnotateCheckBox.IsChecked.HasValue && AnnotateCheckBox.IsChecked.Value,
                    TextBoxNamespace.Text, TextBoxTableName.Text, table,
                    NullableCheckBox.IsChecked.HasValue && NullableCheckBox.IsChecked.Value);
                box1.Text = str;

            }
            catch (Exception exception)
            {
                ConfigWindow.SyntaxProvider.UnloadLanguage();
                ConfigWindow.SyntaxProvider.InitializeSyntaxHighlighter(box1, true);
                box1.Text = exception.GetExceptionMessages();
            }
        }
    }
}
