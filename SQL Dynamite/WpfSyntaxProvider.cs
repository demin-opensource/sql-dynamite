﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using SqlDynamite.Common;
using SqlDynamite.Util;

namespace SqlDynamite
{
    public class WpfSyntaxProvider : BaseSyntaxProvider
    {
        public readonly Color FunctionColor = Color.Magenta;
        public readonly Color FunctionNoAnsiColor = Color.Salmon;
        public readonly Color SearchColor = Color.Red;
        public readonly Color SearchBackgroundColor = Color.LightYellow;

        private readonly Color KeywordColor = Color.Blue;
        private readonly Color TypeColor = Color.Purple;
        private readonly Dictionary<string, Color> TagsList = new Dictionary<string, Color>();

        public void LoadLanguage(string language)
        {
            string[] keywords = null;
            string[] types = null;
            if (language == "C#")
            {
                keywords = csKeywords;
                types = csTypes;
            }
            else if (language == "F#")
            {
                keywords = fsKeywords;
                types = fsTypes;
                TagsList["Nullable"] = Color.Gray;
                foreach (string str in new []{ "array", "list", "option", "ref", "seq" })
                {
                    if (!TagsList.ContainsKey(str))
                    {
                        TagsList[str] = FunctionColor;
                    }
                }
            }
            else if (language == "VB.NET")
            {
                keywords = vbKeywords;
                types = vbTypes;
            }
            else if (language == "JavaScript")
            {
                keywords = jsKeywords;
                types = jsProperties;
            }
            else if (language == "CQL")
            {
                keywords = cqlKeywords;
                types = cqlTypes;
            }

            foreach (string str in keywords)
            {
                if (!TagsList.ContainsKey(str))
                {
                    TagsList[str] = KeywordColor;
                }
            }

            foreach (string str in types)
            {
                if (!TagsList.ContainsKey(str))
                {
                    TagsList[str] = TypeColor;
                }
            }
        }

        public void UnloadLanguage()
        {
            TagsList.Clear();
        }

        public void LoadWords(string words, Color color)
        {
            string[] strs = words.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);

            for (int index = 0; index < strs.Length; index++)
            {
                strs[index] = strs[index].Replace("\r", "");
            }

            foreach (string str in strs)
            {
                if (!TagsList.ContainsKey(str.ToLower()) && !TagsList.ContainsKey(str.ToUpper()))
                {
                    TagsList[str.ToLower()] = color;
                    TagsList[str.ToUpper()] = color;
                }
            }
        }

        public void UnloadWords()
        {
            var indexesToDelete =
                TagsList.Where(
                    pair =>
                    pair.Value == FunctionNoAnsiColor).
                    ToList();
            foreach (KeyValuePair<string, Color> pair in indexesToDelete)
            {
                TagsList.Remove(pair.Key);
            }
        }

        public void InitializeSyntaxHighlighter(SyntaxHighlightingTextBox box, bool syntaxHighlighting)
        {
            box.Separators.Clear();
            box.HighlightDescriptors.Clear();

            foreach (char separator in Separators)
            {
                box.Separators.Add(separator);
            }

            if (syntaxHighlighting)
            {
                foreach (KeyValuePair<string, Color> tag in TagsList)
                {
                    box.HighlightDescriptors.Add(new HighlightDescriptor(tag.Key, tag.Value, null, DescriptorType.Word,
                                                                          DescriptorRecognition.WholeWord));
                }

                box.HighlightDescriptors.Add(new HighlightDescriptor("/*", "*/", Color.Gray, null,
                                                                      DescriptorType.ToCloseToken,
                                                                      DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("--", Color.Gray, null,
                                                                      DescriptorType.ToEOL,
                                                                      DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("//", Color.Gray, null,
                                                                      DescriptorType.ToEOL,
                                                                      DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("@", Color.MediumPurple, null,
                                                                      DescriptorType.Word,
                                                                      DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("#", Color.Brown, null,
                                                                      DescriptorType.Word,
                                                                      DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("'", Color.SandyBrown, null,
                                                                      DescriptorType.Word,
                                                                      DescriptorRecognition.Contains));
                box.HighlightDescriptors.Add(new HighlightDescriptor("`", Color.SandyBrown, null,
                                                                      DescriptorType.Word,
                                                                      DescriptorRecognition.Contains));
                box.HighlightDescriptors.Add(new HighlightDescriptor("\"", Color.SandyBrown, null,
                                                                      DescriptorType.Word,
                                                                      DescriptorRecognition.Contains));
                #region Numeric
                box.HighlightDescriptors.Add(new HighlightDescriptor("0", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("1", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("2", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("3", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("4", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("5", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("6", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("7", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("8", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("9", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                box.HighlightDescriptors.Add(new HighlightDescriptor("-", Color.Green, null, DescriptorType.Word, DescriptorRecognition.StartsWith));
                #endregion
            }
        }

        public void SetKeywordColors(MetadataFinder finder)
        {
            if (finder is SsasMetadataFinder ssasMetadataFinder && ssasMetadataFinder._connection is WrapperConnection)
            {
                var schemaReservedWords = ssasMetadataFinder.GetKeywords().Tables[0];
                var keywords = from DataRow row in schemaReservedWords.Rows select row[0].ToString();
                LoadWords(string.Join(Environment.NewLine, keywords), KeywordColor);
                var schemaFunctions = ssasMetadataFinder.GetFunctions().Tables[0];
                var functions = from DataRow row in schemaFunctions.Rows select row[0].ToString();
                LoadWords(string.Join(Environment.NewLine, functions), FunctionColor);
            }
            else
            {
                var schema = finder.GetConnection().GetSchema();
                foreach (DataRow dataRow in schema.Rows)
                {
                    if (dataRow[0].Equals("DataTypes"))
                    {
                        var schemaDataTypes = finder.GetConnection().GetSchema("DataTypes");
                        var dataTypes = from DataRow row in schemaDataTypes.Rows select row[0].ToString();
                        LoadWords(string.Join(Environment.NewLine, dataTypes), TypeColor);
                    }
                    else if (dataRow[0].Equals("ReservedWords"))
                    {
                        var schemaReservedWords = finder.GetConnection().GetSchema("ReservedWords");
                        var keywords = from DataRow row in schemaReservedWords.Rows select row[0].ToString();
                        LoadWords(string.Join(Environment.NewLine, keywords), KeywordColor);
                    }
                }
            }
        }
    }
}