using System;
using NUnit.Framework;
using SqlDynamite.Common;

namespace SqlDynamite.Test
{
    [TestFixture]
    public class MetadataFinderTest
    {
        [Test]
        public void MsSql_Net_Test()
        {
            var mf = new MsSqlMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "data source=localhost;initial catalog=test;integrated security=False;user id=user;password=pswd");
        }

        [Test]
        public void MsSql_Odbc_Test()
        {
            var mf = new MsSqlMetadataFinder();
            var connectionString = mf.MakeConnectionString("ODBC Driver 13 for SQL Server", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={ODBC Driver 13 for SQL Server};Server=localhost;Database=test;Uid=user;Pwd=pswd");
        }

        [Test]
        public void MsSql_OleDb_Test()
        {
            var mf = new MsSqlMetadataFinder();
            var connectionString = mf.MakeConnectionString("Microsoft OLE DB Provider for SQL Server", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=Microsoft OLE DB Provider for SQL Server;Server=localhost;Database=test;User Id=user;Password=pswd;Trusted_Connection=No");
        }

        [Test]
        public void Oracle_Net_Test()
        {
            var mf = new OracleMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=test)));User Id=user;Password=pswd;");
        }

        [Test]
        public void Oracle_Odbc_Test()
        {
            var mf = new OracleMetadataFinder();
            var connectionString = mf.MakeConnectionString("Oracle in instantclient_19_3", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={Oracle in instantclient_19_3};Server=localhost;Port=1521;Database=test;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Oracle_OleDb_Test()
        {
            var mf = new OracleMetadataFinder();
            var connectionString = mf.MakeConnectionString("Oracle Provider for OLE DB", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=Oracle Provider for OLE DB;Data Source=(DESCRIPTION=(CID=GTU_APP)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521)))(CONNECT_DATA=(SID=test)(SERVER=DEDICATED)));User Id=user;Password=pswd;");
        }

        [Test]
        public void PostgreSql_Net_Test()
        {
            var mf = new PostgreSqlMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Server=localhost;Database=test;Uid=user;Pwd=pswd;integrated security=False");
        }

        [Test]
        public void PostgreSql_Odbc_Test()
        {
            var mf = new PostgreSqlMetadataFinder();
            var connectionString = mf.MakeConnectionString("PostgreSQL Unicode", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={PostgreSQL Unicode};Server=localhost;Database=test;Uid=user;Pwd=pswd");
        }

        [Test]
        public void PostgreSql_OleDb_Test()
        {
            var mf = new PostgreSqlMetadataFinder();
            var connectionString = mf.MakeConnectionString("OLE DB Provider for PostgreSQL", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=OLE DB Provider for PostgreSQL;Data Source=localhost;Initial Catalog=test;User Id=user;Password=pswd");
        }

        [Test]
        public void Db2_Net_Test()
        {
            var mf = new Db2MetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Server=localhost;Database=test;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Db2_Odbc_Test()
        {
            var mf = new Db2MetadataFinder();
            var connectionString = mf.MakeConnectionString("IBM DB2 ODBC DRIVER", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={IBM DB2 ODBC DRIVER};Hostname=localhost;Protocol=TCPIP;Database=test;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Db2_OleDb_Test()
        {
            var mf = new Db2MetadataFinder();
            var connectionString = mf.MakeConnectionString("IBM OLE DB Provider for DB2", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=IBM OLE DB Provider for DB2;Hostname=localhost;Protocol=TCPIP;Database=test;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Ase_Net_Test()
        {
            var mf = new SybaseMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Data Source=localhost;Database=test;charset=utf8;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Ase_Odbc_Test()
        {
            var mf = new SybaseMetadataFinder();
            var connectionString = mf.MakeConnectionString("Adaptive Server Enterprise", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={Adaptive Server Enterprise};Server=localhost;charset=utf8;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Ase_OleDb_Test()
        {
            var mf = new SybaseMetadataFinder();
            var connectionString = mf.MakeConnectionString("Sybase OLEDB Provider", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=Sybase OLEDB Provider;Server=localhost;Initial Catalog=test;User Id=user;Password=pswd");
        }

        [Test]
        public void Anywhere_Net_Test()
        {
            var mf = new SybaseAnywhereMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "LINKS=TCPIP{IP=localhost};ENG=test;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Anywhere_Odbc_Test()
        {
            var mf = new SybaseAnywhereMetadataFinder();
            var connectionString = mf.MakeConnectionString("SQL Anywhere 17", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={SQL Anywhere 17};LINKS=TCPIP{HOST=localhost};ENG=test;uid=user;pwd=pswd");
        }

        [Test]
        public void Anywhere_OleDb_Test()
        {
            var mf = new SybaseAnywhereMetadataFinder();
            var connectionString = mf.MakeConnectionString("SQL Anywhere OLE DB Provider 17", "demo17", "demo", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=SQL Anywhere OLE DB Provider 17;Server=demo17;Initial Catalog=demo;User Id=user;Password=pswd");
        }

        [Test]
        public void Advantage_Net_Test()
        {
            var mf = new SybaseAdvantageMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, string.Empty, @"c:\data\demo.add", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, @"Data Source=c:\data\demo.add;User ID=user;Password=pswd;ServerType=LOCAL|REMOTE|AIS;");
        }

        [Test]
        public void Advantage_Odbc_Test()
        {
            var mf = new SybaseAdvantageMetadataFinder();
            var connectionString = mf.MakeConnectionString("Advantage StreamlineSQL ODBC", string.Empty, @"c:\data\demo.add", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, @"Driver={Advantage StreamlineSQL ODBC};DataDirectory=c:\data\demo.add;User ID=user;Password=pswd;ServerTypes=7;");
        }

        [Test]
        public void Advantage_OleDb_Test()
        {
            var mf = new SybaseAdvantageMetadataFinder();
            var connectionString = mf.MakeConnectionString("Advantage OLE DB Provider", string.Empty, @"c:\data\demo.add", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, @"Provider=Advantage OLE DB Provider;Data Source=c:\data\demo.add;User ID=user;Password=pswd;Advantage Server Type=ADS_LOCAL_SERVER|ADS_REMOTE_SERVER;");
        }

        [Test]
        public void Firebird_Net_Test()
        {
            var clientLib = Environment.OSVersion.Platform == PlatformID.Win32NT ? "fbclient.dll" : "libfbclient.so";
            var mf = new FirebirdMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", @"c:\test.fdb", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, @"DataSource=localhost;Database=c:\test.fdb;User=user;Password=pswd;Client=" + clientLib);
        }

        [Test]
        public void Firebird_Odbc_Test()
        {
            var clientLib = Environment.OSVersion.Platform == PlatformID.Win32NT ? "fbclient.dll" : "libfbclient.so";
            var mf = new FirebirdMetadataFinder();
            var connectionString = mf.MakeConnectionString("Firebird/InterBase(r) driver", "localhost", @"c:\test.fdb", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, @"Driver={Firebird/InterBase(r) driver};DbName=localhost:c:\test.fdb;Uid=user;Pwd=pswd;Client=" + clientLib);
        }

        [Test]
        public void Firebird_OleDb_Test()
        {
            var mf = new FirebirdMetadataFinder();
            var connectionString = mf.MakeConnectionString("LCPI.IBProvider.5", "localhost", @"c:\test.fdb", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, @"Provider=LCPI.IBProvider.5;Location=localhost:c:\test.fdb;User Id=user;Password=pswd;auto_commit=true");
        }

        [Test]
        public void Interbase_Net_Test()
        {
            var mf = new InterbaseMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", @"c:\test.gdb", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, @"DriverName=Interbase;Database=localhost:c:\test.gdb;RoleName=RoleName;User_Name=user;Password=pswd;SQLDialect=3;GetDriverFunc=getSQLDriverINTERBASE;LibraryName=dbxint30.dll;VendorLib=GDS32.DLL");
        }

        [Test]
        public void Interbase_Odbc_Test()
        {
            var mf = new InterbaseMetadataFinder();
            var connectionString = mf.MakeConnectionString("Firebird/InterBase(r) driver", "localhost", @"c:\test.gdb", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, @"Driver={Firebird/InterBase(r) driver};DbName=localhost:c:\test.gdb;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Interbase_OleDb_Test()
        {
            var mf = new InterbaseMetadataFinder();
            var connectionString = mf.MakeConnectionString("LCPI.IBProvider.5", "localhost", @"c:\test.gdb", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, @"Provider=LCPI.IBProvider.5;Location=localhost:c:\test.gdb;User Id=user;Password=pswd;auto_commit=true");
        }

        [Test]
        public void Sqlite_Net_Test()
        {
            var mf = new SqliteMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, string.Empty, @"c:\test.db", null, null, false, ConnectionType.NET);
            Assert.AreEqual(connectionString, @"Data Source=c:\test.db;Version=3;");
        }

        [Test]
        public void Sqlite_Odbc_Test()
        {
            var mf = new SqliteMetadataFinder();
            var connectionString = mf.MakeConnectionString("SQLite ODBC Driver", string.Empty, @"c:\test.db", null, null, false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, @"Driver={SQLite ODBC Driver};Data Source=c:\test.db;Version=3;");
        }

        [Test]
        public void Sqlite_OleDb_Test()
        {
            var mf = new SqliteMetadataFinder();
            var connectionString = mf.MakeConnectionString("OLE DB Provider for SQLite databases", string.Empty, @"c:\test.db", null, null, false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, @"Provider=OLE DB Provider for SQLite databases;Data Source=c:\test.db;Version=3;");
        }

        [Test]
        public void Informix_Net_Test()
        {
            var mf = new InformixMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost:9088", "myserver/sysmaster", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Host=localhost;Server=myserver;UID=user;Pwd=pswd;Service=9088;Database=sysmaster");
        }

        [Test]
        public void Informix_Odbc_Test()
        {
            var mf = new InformixMetadataFinder();
            var connectionString = mf.MakeConnectionString("IBM INFORMIX ODBC DRIVER", "localhost:9088", "myserver/sysmaster", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={IBM INFORMIX ODBC DRIVER};Host=localhost;Server=myserver;UID=user;Pwd=pswd;Protocol=olsoctcp;Service=9088;Database=sysmaster");
        }

        [Test]
        public void Informix_OleDb_Test()
        {
            var mf = new InformixMetadataFinder();
            var connectionString = mf.MakeConnectionString("IBM OLE DB PROVIDER FOR INFORMIX", "myserver", "sysmaster", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=IBM OLE DB PROVIDER FOR INFORMIX;Data Source=sysmaster@myserver;User ID=user;Password=pswd");
        }

        [Test]
        public void Ingres_Net_Test()
        {
            var mf = new IngresMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Host=localhost;Database=test;User Id=user;PWD=pswd");
        }

        [Test]
        public void Ingres_Odbc_Test()
        {
            var mf = new IngresMetadataFinder();
            var connectionString = mf.MakeConnectionString("Ingres", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "DRIVER=Ingres;SRVR=localhost;DB=test;Persist Security Info=False;Uid=user;Pwd=pswd;SELECTLOOPS=N;");
        }

        [Test]
        public void MySql_Net_Test()
        {
            var mf = new MySqlMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Server=localhost;Database=test;Uid=user;Pwd=pswd;");
        }

        [Test]
        public void MySql_Odbc_Test()
        {
            var mf = new MySqlMetadataFinder();
            var connectionString = mf.MakeConnectionString("MySQL ODBC 5.3 Unicode Driver", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={MySQL ODBC 5.3 Unicode Driver};Server=localhost;User=user;Password=pswd;Option=3");
        }

        [Test]
        public void Vertica_Net_Test()
        {
            var mf = new VerticaMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Host=localhost;Database=test;User=user;Password=pswd;");
        }

        [Test]
        public void Vertica_Odbc_Test()
        {
            var mf = new VerticaMetadataFinder();
            var connectionString = mf.MakeConnectionString("Vertica", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={Vertica};Host=localhost;Database=test;User=user;Password=pswd;");
        }

        [Test]
        public void Vertica_OleDb_Test()
        {
            var mf = new VerticaMetadataFinder();
            var connectionString = mf.MakeConnectionString("Vertica OLE DB Provider", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=Vertica OLE DB Provider;Host=localhost;Initial Catalog=test;User=user;Password=pswd;");
        }

        [Test]
        public void SqlAzure_Net_Test()
        {
            var mf = new SqlAzureMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Server=tcp:localhost;Database=test;Trusted_Connection=False;User ID=user@localhost;Password=pswd;Encrypt=True;");
        }

        [Test]
        public void SqlAzure_Odbc_Test()
        {
            var mf = new SqlAzureMetadataFinder();
            var connectionString = mf.MakeConnectionString("ODBC Driver 13 for SQL Server", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={ODBC Driver 13 for SQL Server};Server=tcp:localhost;Database=test;Uid=user@localhost;Pwd=pswd;Encrypt=yes");
        }

        [Test]
        public void SqlAzure_OleDb_Test()
        {
            var mf = new SqlAzureMetadataFinder();
            var connectionString = mf.MakeConnectionString("Microsoft OLE DB Provider for SQL Server", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=Microsoft OLE DB Provider for SQL Server;Data Source=tcp:localhost;Initial Catalog=test;User Id=user;Password=pswd");
        }

        [Test]
        public void Ultralite_Net_Test()
        {
            var mf = new UltraliteMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, string.Empty, @"c:\test.udb", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, @"Dbf=c:\test.udb;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Ultralite_Odbc_Test()
        {
            var mf = new UltraliteMetadataFinder();
            var connectionString = mf.MakeConnectionString("UltraLite 17", string.Empty, @"c:\test.udb", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, @"Driver={UltraLite 17};Dbf=c:\test.udb;Uid=user;Pwd=pswd");
        }

        [Test]
        public void Cassandra_Net_Test()
        {
            var mf = new CassandraMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Default Keyspace=test;Contact Points=localhost;Username=user;Password=pswd");
        }

        [Test]
        public void MongoDb_Net_Test()
        {
            var mf = new MongoDbMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "mongodb://user:pswd@localhost/test");
        }

        [Test]
        public void Redis_Net_Test()
        {
            var mf = new RedisMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", null, "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "localhost;test;Password=pswd");
        }

        [Test]
        public void Hana_Net_Test()
        {
            var mf = new SapHanaMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost:39015", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "Server=localhost:39015;databaseName=test;UserID=user;Password=pswd");
        }

        [Test]
        public void Hana_Odbc_Test()
        {
            var mf = new SapHanaMetadataFinder();
            var connectionString = mf.MakeConnectionString("HDBODBC", "localhost:39015", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver={HDBODBC};SERVERNODE=localhost:39015;databaseName=test;UID=user;PWD=pswd");
        }

        [Test]
        public void Neo4j_Net_Test()
        {
            var mf = new Neo4jMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost:7687", null, "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "bolt://localhost:7687;user;pswd");
        }

        [Test]
        public void InfluxDb_Net_Test()
        {
            var mf = new InfluxDbMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost:8086", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "http://localhost:8086;user;pswd;test");
        }

        [Test]
        public void Ssas_Net_Test()
        {
            var mf = new SsasMetadataFinder();
            var connectionString = mf.MakeConnectionString(null, "localhost", "test", "user", "pswd", false, ConnectionType.NET);
            Assert.AreEqual(connectionString, "data source=localhost;initial catalog=test;user id=user;password=pswd;");
        }

        [Test]
        public void Ssas_Odbc_Test()
        {
            var mf = new SsasMetadataFinder();
            var connectionString = mf.MakeConnectionString("ODBC Driver for SQL Server Analysis Services", "localhost", "test", "user", "pswd", false, ConnectionType.ODBC);
            Assert.AreEqual(connectionString, "Driver=ODBC Driver for SQL Server Analysis Services;Url=localhost;initial catalog=test;user id=user;password=pswd;");
        }

        [Test]
        public void Ssas_OleDb_Test()
        {
            var mf = new SsasMetadataFinder();
            var connectionString = mf.MakeConnectionString("Microsoft OLE DB Provider for Analysis Services 14.0", "localhost", "test", "user", "pswd", false, ConnectionType.OLEDB);
            Assert.AreEqual(connectionString, "Provider=Microsoft OLE DB Provider for Analysis Services 14.0;data source=localhost;initial catalog=test;user id=user;password=pswd;");
        }
    }
}
