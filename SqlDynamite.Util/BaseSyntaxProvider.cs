﻿using System;

namespace SqlDynamite.Util
{
    public abstract class BaseSyntaxProvider
    {
        public readonly char[] Separators =
        {
            ' ', '\n', '\r', '\t', ',', '.', '+', '=', '<', '>', '(', ')', '[', ']',
            ';', ':', '?', '!'
        };

        protected readonly string[] csKeywords =
        {
            "abstract", "as", "base", "break", "case", "catch", "checked", "class", "const", "continue", "default",
            "delegate", "do", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed", "for",
            "foreach", "goto", "if", "implicit", "in", "interface", "internal", "is", "lock", "namespace", "new",
            "null", "operator", "out", "override", "params", "private", "protected", "public", "readonly",
            "ref", "return", "sealed", "sizeof", "stackalloc", "static", "struct", "switch", "this", "throw", "true",
            "try", "typeof", "unchecked", "unsafe", "using", "virtual", "volatile", "while", "add", "alias",
            "ascending", "async", "await", "descending", "dynamic", "from", "get", "global", "group", "into", "join",
            "let", "orderby", "partial", "remove", "select", "set", "value", "var", "where", "yield"
        };

        protected readonly string[] csTypes =
        {
            "bool", "byte", "sbyte", "short", "ushort", "int", "uint", "long", "ulong", "char", "decimal", "double",
            "float",
            "void", "string", "DateTime", "SqlGeometry", "SqlGeography", "SqlHierarchyId", "TimeSpan", "Guid", "object"
        };

        protected readonly string[] vbKeywords =
        {
            "AddHandler", "AddressOf", "Alias", "And", "AndAlso", "As", "ByRef", "ByVal", "Call", "Case", "Catch",
            "Class", "Const", "Continue", "CType", "Declare", "Default", "Delegate", "Dim", "DirectCast", "Do",
            "Each", "Else", "ElseIf", "End", "EndIf", "Enum", "Erase", "Error", "Event", "Exit", "False", "Finally",
            "For", "Friend", "Function", "Get", "GetType", "GetXMLNamespace", "Global", "GoSub", "GoTo", "Handles",
            "If", "Implements", "Imports", "In", "Inherits", "Integer", "Interface", "Is", "IsNot", "Let", "Lib",
            "Like", "Loop", "Me", "Mod", "Module", "MustInherit", "MustOverride", "MyBase", "MyClass", "Namespace",
            "Narrowing", "New", "Next", "Not", "Nothing", "NotInheritable", "NotOverridable", "Of", "On", "Operator",
            "Option", "Optional", "Or", "OrElse", "Out", "Overloads", "Overridable", "Overrides", "ParamArray",
            "Partial", "Private", "Property", "Protected", "Public", "RaiseEvent", "ReadOnly", "ReDim", "REM",
            "RemoveHandler", "Resume", "Return", "Select", "Set", "Shadows", "Shared", "Static", "Step", "Stop",
            "String", "Structure", "Sub", "SyncLock", "Then", "Throw", "To", "True", "Try", "TryCast", "TypeOf",
            "Using", "Wend", "When", "While", "Widening", "With", "WithEvents", "WriteOnly", "Xor", "#Const",
            "#Else", "#ElseIf", "#End", "#If", "Aggregate", "Ansi", "Assembly", "Async", "Auto", "Await", "Binary",
            "Compare", "Custom", "Distinct", "Equals", "Explicit", "From", "By", "Group", "Into", "IsFalse",
            "IsTrue", "Iterator", "Join", "Key", "Mid", "Off", "Order", "Preserve", "Skip", "Skip While", "Strict",
            "Take", "Text", "Unicode", "Until", "Where", "Yield ", "#ExternalSource", "#Region"
        };

        protected readonly string[] vbTypes =
        {
            "Boolean", "Byte", "CBool", "CByte", "CChar", "CDate", "CDbl", "CDec", "Char", "CInt", "CLng", "CObj",
            "CSByte", "CShort", "CSng", "CStr", "CUInt", "CULng", "CUShort", "Date", "Decimal", "Double", "Long",
            "Object", "SByte", "Short", "Single", "UInteger", "ULong", "UShort", "Variant", "SqlGeometry",
            "SqlGeography", "SqlHierarchyId", "TimeSpan", "Guid"
        };

        protected readonly string[] fsKeywords =
        {
            "asr", "land", "lor", "lsl", "lsr", "lxor", "mod", "sig", "atomic", "break", "checked", "component",
            "const", "constraint", "constructor", "continue", "eager", "event", "external", "fixed", "functor",
            "include", "method", "mixin", "object", "parallel", "process", "protected", "pure", "sealed", "tailcall",
            "trait", "virtual", "volatile", "abstract", "and", "as", "assert", "base", "begin", "class", "default",
            "delegate", "do", "done", "downcast", "downto", "elif", "else", "end", "exception", "extern", "FALSE",
            "finally", "for", "fun", "function", "global", "if", "in", "inherit", "inline", "interface", "internal",
            "lazy", "let", "match", "member", "module", "mutable", "namespace", "new", "not", "null", "of", "open",
            "or", "override", "private", "public", "rec", "return", "select", "static", "struct", "then", "to",
            "TRUE", "try", "type", "upcast", "use", "val", "void", "when", "while", "with", "yield"
        };

        protected readonly string[] fsTypes =
        {
            "bool", "byte", "sbyte", "int16", "uint16", "int", "uint32", "int64", "uint64", "nativeint",
            "unativeint", "char", "string", "decimal", "unit", "void", "float32, single", "float, double",
            "DateTime", "SqlGeometry", "SqlGeography", "SqlHierarchyId",
            "TimeSpan", "Guid"
        };

        protected readonly string[] jsKeywords =
        {
            "abstract", "arguments", "boolean", "break", "byte",
            "case", "catch", "char", "class", "const",
            "continue", "debugger", "default", "delete", "do",
            "double", "else", "enum", "eval", "export",
            "extends", "false", "final", "finally", "float",
            "for", "function", "goto", "if", "implements",
            "import", "in", "instanceof", "int", "interface",
            "let", "long", "native", "new", "null",
            "package", "private", "protected", "public", "return",
            "short", "static", "super", "switch", "synchronized",
            "this", "throw", "throws", "transient", "true",
            "try", "typeof", "var", "void", "volatile",
            "while", "with", "yield"
        };

        protected readonly string[] jsProperties =
        {
            "Array", "Date", "eval", "function", "hasOwnProperty",
            "Infinity", "isFinite", "isNaN", "isPrototypeOf", "length",
            "Math", "NaN", "name", "Number", "Object",
            "prototype", "String", "toString", "undefined", "valueOf"
        };

        protected readonly string[] cqlKeywords =
        {
            "ALL", "AS", "CLUSTERING", "COMPACT", "CONSISTENCY", "COUNT",
            "DISTINCT", "EXISTS", "WRITETIME", "VALUES", "USER", "USERS", "FILTERING", "TTL", "NOSUPERUSER",
            "PERMISSION", "PERMISSIONS", "STATIC", "STORAGE", "SUPERUSER", "KEY", "LEVEL", "TYPE", "ADD", "AGGREGATE",
            "ALLOW", "ALTER", "AND", "ANY", "APPLY", "ASC", "AUTHORIZE", "BATCH", "BEGIN", "BY", "COLUMNFAMILY",
            "CREATE", "DELETE", "DESC", "DROP", "EACH_QUORUM", "ENTRIES", "FROM", "FULL", "GRANT", "IF", "IN", "INDEX",
            "INFINITY", "INSERT", "INTO", "KEYSPACE", "KEYSPACES", "LIMIT", "LOCAL_ONE", "LOCAL_QUORUM",
            "MATERIALIZED", "MODIFY", "NAN", "NORECURSIVE", "NOT", "OF", "ON", "ONE", "ORDER", "PARTITION", "PASSWORD",
            "PER", "PRIMARY", "QUORUM", "RENAME", "REVOKE", "SCHEMA", "SELECT", "TABLE", "THREE", "TO", "TOKEN",
            "TRUNCATE", "TWO", "UNLOGGED", "UPDATE", "USE", "USING", "VIEW", "WHERE", "WITH"
        };

        protected readonly string[] cqlTypes =
        {
            "ASCII", "BIGINT", "BLOB", "BOOLEAN",
            "COUNTER", "CUSTOM", "DECIMAL", "DOUBLE", 
            "FLOAT", "FROZEN", "INT", "LIST", "MAP",
            "TEXT", "TIMESTAMP", "TIMEUUID", "TUPLE",
            "UUID", "VARCHAR", "VARINT", "SET", "INET",
            "DATE", "TIME"
        };
    }
}
