﻿using System;
using System.Data;
using System.Linq;
using System.Text;

namespace SqlDynamite.Util
{
    public static class ClassGenerator
    {
        private static string GetColumnTypeName(string lang, DataColumn dataColumn, bool nullable)
        {
            string typeName = null;
            if (lang == "C#")
            {
                var typeArray1 = new[] {"String", "Decimal", "Double", "Byte", "Byte[]", "Object"};
                var typeArray2 = new[] {"string", "byte[]", "SqlGeometry", "SqlGeography", "object", "IPAddress", "LocalDate", "LocalTime"};
                typeName = dataColumn.DataType.Name;
                if (typeArray1.Contains(typeName))
                {
                    typeName = typeName.ToLower();
                }
                switch (typeName)
                {
                    case "Int16":
                        typeName = "short";
                        break;
                    case "Int32":
                        typeName = "int";
                        break;
                    case "Int64":
                        typeName = "long";
                        break;
                    case "Single":
                        typeName = "float";
                        break;
                    case "Boolean":
                        typeName = "bool";
                        break;
                }
                if (dataColumn.AllowDBNull && !typeArray2.Contains(typeName) && nullable)
                {
                    typeName += "?";
                }
                if (typeName == "LocalDate" || typeName == "LocalTime")
                {
                    typeName = "Cassandra." + typeName;
                }
                if (typeName.StartsWith("Sql", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = "Microsoft.SqlServer.Types." + typeName;
                }
                if (typeName == "IPAddress")
                {
                    typeName = "System.Net.IPAddress";
                }
                if (typeName.StartsWith("BigInteger", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = "System.Numerics." + typeName;
                }
                if (typeName.StartsWith("Tuple", StringComparison.OrdinalIgnoreCase) || 
                    typeName.StartsWith("IDictionary", StringComparison.OrdinalIgnoreCase) ||
                    typeName.StartsWith("IEnumerable", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = GetGenericArguments(lang, dataColumn.DataType, nullable);
                }
            }
            else if (lang == "F#")
            {
                var typeArray1 = new[] {"String", "Decimal", "Byte", "Int16", "Int64", "Single"};
                var typeArray2 = new[]
                {
                    "string", "byte array", "Microsoft.SqlServer.Types.SqlGeometry",
                    "Microsoft.SqlServer.Types.SqlGeography", "obj", "System.Net.IPAddress",
                    "Cassandra.LocalDate", "Cassandra.LocalTime"
                };
                typeName = dataColumn.DataType.Name;
                if (typeArray1.Contains(typeName))
                {
                    typeName = typeName.ToLower();
                }
                else
                {
                    switch (typeName)
                    {
                        case "Int32":
                            typeName = "int";
                            break;
                        case "Double":
                            typeName = "float";
                            break;
                        case "Boolean":
                            typeName = "bool";
                            break;
                        case "Byte[]":
                            typeName = "byte array";
                            break;
                        case "Object":
                            typeName = "obj";
                            break;
                    }
                }
                if (typeName == "LocalDate" || typeName == "LocalTime")
                {
                    typeName = "Cassandra." + typeName;
                }
                if (typeName.StartsWith("Sql", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = "Microsoft.SqlServer.Types." + typeName;
                }
                if (typeName == "IPAddress")
                {
                    typeName = "System.Net.IPAddress";
                }
                if (typeName == "BigInteger")
                {
                    typeName = "System.Numerics.BigInteger";
                }
                if (typeName.StartsWith("Tuple", StringComparison.OrdinalIgnoreCase) || 
                    typeName.StartsWith("IDictionary", StringComparison.OrdinalIgnoreCase) ||
                    typeName.StartsWith("IEnumerable", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = GetGenericArguments(lang, dataColumn.DataType, nullable);
                }
                else if (dataColumn.AllowDBNull && !typeArray2.Contains(typeName))
                {
                    typeName = nullable ? "Nullable<" + typeName + ">" : typeName;
                }
            }
            else if (lang == "VB.NET")
            {
                var typeArray2 = new[] {"String", "Byte()", "SqlGeometry", "SqlGeography", "Object", "IPAddress", "LocalDate", "LocalTime"};
                typeName = dataColumn.DataType.Name;
                switch (typeName)
                {
                    case "DateTime":
                        typeName = "Date";
                        break;
                    case "Int16":
                        typeName = "Short";
                        break;
                    case "Int32":
                        typeName = "Integer";
                        break;
                    case "Int64":
                        typeName = "Long";
                        break;
                    case "Byte[]":
                        typeName = "Byte()";
                        break;
                }
                if (dataColumn.AllowDBNull && !typeArray2.Contains(typeName) && nullable)
                {
                    typeName += "?";
                }
                if (typeName == "LocalDate" || typeName == "LocalTime")
                {
                    typeName = "Cassandra." + typeName;
                }
                if (typeName.StartsWith("Sql", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = "Microsoft.SqlServer.Types." + typeName;
                }
                if (typeName == "IPAddress")
                {
                    typeName = "System.Net.IPAddress";
                }
                if (typeName.StartsWith("BigInteger", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = "System.Numerics." + typeName;
                }
                if (typeName.StartsWith("Tuple", StringComparison.OrdinalIgnoreCase) || 
                    typeName.StartsWith("IDictionary", StringComparison.OrdinalIgnoreCase) ||
                    typeName.StartsWith("IEnumerable", StringComparison.OrdinalIgnoreCase))
                {
                    typeName = GetGenericArguments(lang, dataColumn.DataType, nullable);
                }
            }
            return typeName;
        }

        private static string GetGenericArguments(string lang, Type type, bool nullable)
        {
            var sb = new StringBuilder();
            string typeName = null;
            if (type.Name.StartsWith("Tuple", StringComparison.OrdinalIgnoreCase))
            {
                typeName = "Tuple";
            }
            else if (type.Name.StartsWith("IDictionary", StringComparison.OrdinalIgnoreCase))
            {
                typeName = "IDictionary";
            }
            else if (type.Name.StartsWith("IEnumerable", StringComparison.OrdinalIgnoreCase))
            {
                typeName = "IEnumerable";
            }
            if (typeName != null)
            {
                if (lang == "C#")
                {
                    sb.Append(typeName + "<");
                    var genericTypeArguments = type.GetGenericArguments();
                    for (int index = 0; index < genericTypeArguments.Length; index++)
                    {
                        sb.Append(GetColumnTypeName(lang, new DataColumn(null, genericTypeArguments[index]), nullable));
                        if (index < genericTypeArguments.Length - 1)
                        {
                            sb.Append(",");
                        }
                    }
                    sb.Append(">");
                }
                else if (lang == "F#")
                {
                    if (type.Name.StartsWith("Tuple", StringComparison.OrdinalIgnoreCase))
                    {
                        sb.Append("(");
                        var genericTypeArguments = type.GetGenericArguments();
                        for (int index = 0; index < genericTypeArguments.Length; index++)
                        {
                            sb.Append(GetColumnTypeName(lang, new DataColumn(null, genericTypeArguments[index]), false));
                            if (index < genericTypeArguments.Length - 1)
                            {
                                sb.Append(" * ");
                            }
                        }
                        sb.Append(")");
                    }
                    else
                    {
                        sb.Append(typeName + "<");
                        var genericTypeArguments = type.GetGenericArguments();
                        for (int index = 0; index < genericTypeArguments.Length; index++)
                        {
                            sb.Append(GetColumnTypeName(lang, new DataColumn(null, genericTypeArguments[index]), nullable));
                            if (index < genericTypeArguments.Length - 1)
                            {
                                sb.Append(",");
                            }
                        }
                        sb.Append(">");
                    }
                }
                else if (lang == "VB.NET")
                {
                    sb.Append(typeName + "(Of ");
                    var genericTypeArguments = type.GetGenericArguments();
                    for (int index = 0; index < genericTypeArguments.Length; index++)
                    {
                        sb.Append(GetColumnTypeName(lang, new DataColumn(null, genericTypeArguments[index]), nullable));
                        if (index < genericTypeArguments.Length - 1)
                        {
                            sb.Append(", ");
                        }
                    }
                    sb.Append(")");
                }
            }
            return sb.ToString();
        }

        public static string GenerateClass(string lang, bool annotateData, string nmspc, string tableName, DataTable dt, bool nullable)
        {
            var sb = new StringBuilder();
            if (lang == "C#")
            {
                sb.Append("using System;\n");
                sb.Append("using System.Collections.Generic;\n");
                if (annotateData) sb.Append("using System.ComponentModel.DataAnnotations.Schema;\n");
                sb.Append("\n");
                sb.Append("namespace " + nmspc + "\n");
                sb.Append("{\n");
                if (annotateData) sb.Append("\t[Table(\"" + tableName + "\")]\n");
                sb.Append("\tpublic class " + tableName + "\n");
                sb.Append("\t{\n");
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    string columnName = dataColumn.ColumnName;
                    string typeName = GetColumnTypeName(lang, dataColumn, nullable);
                    if (annotateData) sb.Append("\t\t[Column(\"" + columnName + "\")]\n");
                    else sb.Append("\n");
                    sb.Append("\t\tpublic " + typeName + " " + columnName);
                    sb.Append(" { get; set; }\n");
                }
                sb.Append("\t}\n");
                sb.Append("}");
            }
            else if (lang == "F#")
            {
                sb.Append("namespace " + nmspc + "\n");
                sb.Append("\n");
                sb.Append("open System\n");
                sb.Append("open System.Collections.Generic\n");
                if (annotateData) sb.Append("open System.ComponentModel.DataAnnotations.Schema\n");
                sb.Append("\n");
                if (annotateData) sb.Append("[<Table(\"" + tableName + "\")>]\n");
                sb.Append("type " + tableName + "()=\n");
                sb.Append("\n");
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    string columnName = dataColumn.ColumnName;
                    string typeName = GetColumnTypeName(lang, dataColumn, nullable);
                    if (annotateData) sb.Append("    [<Column(\"" + columnName + "\")>]\n");
                    else sb.Append("    \n");
                    sb.Append("    member val " + columnName + " : " + typeName + " = Unchecked.defaultof<" + typeName + ">");
                    sb.Append(" with get, set\n");
                }
                sb.Append("\n");
            }
            else if (lang == "VB.NET")
            {
                sb.Append("Imports System\n");
                sb.Append("Imports System.Collections.Generic\n");
                if (annotateData) sb.Append("Imports System.ComponentModel.DataAnnotations.Schema\n");
                sb.Append("\n");
                sb.Append("Namespace " + nmspc + "\n");
                sb.Append("\n");
                if (annotateData) sb.Append("\t<Table(\"" + tableName + "\")>\n");
                sb.Append("\tPublic Class " + tableName + "\n");
                sb.Append("\t\n");
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    string columnName = dataColumn.ColumnName;
                    string typeName = GetColumnTypeName(lang, dataColumn, nullable);
                    if (annotateData) sb.Append("\t\t<Column(\"" + columnName + "\")>\n");
                    else sb.Append("\n");
                    sb.Append("\t\tPublic Property " + columnName + " As " + typeName + "\n");
                }
                sb.Append("\t\n");
                sb.Append("\tEnd Class\n");
                sb.Append("\n");
                sb.Append("End Namespace\n");
            }
            return sb.ToString();
        }
    }
}
