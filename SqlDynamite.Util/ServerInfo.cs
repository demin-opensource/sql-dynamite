﻿using System;

namespace SqlDynamite.Util
{
    public class ServerInfo
    {
        public ServerInfo(string serverType, string driver, string server, string database, string user, string password,
            string sspi, string connectionType, string name)
        {
            ServerType = serverType;
            Driver = driver;
            Server = server;
            Database = database;
            User = user;
            Password = password;
            SSPI = sspi;
            ConnectionType = connectionType;
            Name = name;
        }

        public string ServerType { get; }
        public string Driver { get; }
        public string Server { get; }
        public string Database { get; }
        public string User { get; }
        public string Password { get; }
        public string SSPI { get; }
        public string ConnectionType { get; }
        public string Name { get; }
    }
}
