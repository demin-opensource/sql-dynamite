﻿#define MSSQL
#define SSAS
#define Sybase_ASE
#define Sybase_Anywhere
#define Sybase_Advantage
#define Sap_Hana
#define Oracle
#define Firebird
#define PostgreSQL
#define MySQL
#define DB2
#define Ingres
#define SQL_Azure
#define RedisCache
#define Sqlite
#define Informix
#define Interbase
#define Sybase_UltraLite
#define Vertica
#define MongoDb
#define Cassandra
#define Redis
#define Neo4j
#define InfluxDb

using System;
using System.Collections.Generic;

using Foundation;
using AppKit;
using SqlDynamite.Common;
using SqlDynamite.Util;

namespace SqlDynamiteM
{
	public partial class ConfigWindowController : NSWindowController
	{
		public WindowController WindowController { get; set; }
		public ListWindowController ListWindowController { get; set; }

		public int DialogResult { get; set; } = 0;

		public string _ConnectionName
		{
			get { return ConnectionName.StringValue; }
			set { ConnectionName.StringValue = value; }
		}
		public string _ServerType
		{
			get { return ServerType.StringValue; }
			set { ServerType.StringValue = value; }
		}
		public string _Driver
		{
			get { return Driver.SelectedValue?.ToString() ?? Driver.StringValue; }
			set { Driver.StringValue = value; }
		}
		public string _Server
		{
			get { return Server.StringValue; }
			set { Server.StringValue = value; }
		}
		public string _Database
		{
			get { return Database.SelectedValue?.ToString() ?? Database.StringValue; }
			set { Database.StringValue = value; }
		}
		public string _User
		{
			get { return User.StringValue; }
			set { User.StringValue = value; }
		}
		public string _Password
		{
			get { return Password.StringValue; }
			set { Password.StringValue = value; }
		}
		public bool _SSPI
		{
			get { return SSPI.State == NSCellStateValue.On; }
			set { SSPI.State = value ? NSCellStateValue.On : NSCellStateValue.Off; }
		}

		public ConnectionType _ConnectionType
		{
			get { return ODBC.State == NSCellStateValue.On ? ConnectionType.ODBC : ConnectionType.NET; }
			set { ODBC.State = value == ConnectionType.ODBC ? NSCellStateValue.On : NSCellStateValue.Off; }
		}

		public ConfigWindowController (IntPtr handle) : base (handle)
		{
		}

		[Export ("initWithCoder:")]
		public ConfigWindowController (NSCoder coder) : base (coder)
		{
		}

		public ConfigWindowController () : base ("ConfigWindow")
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			IEnumerable<string> drivers = DataUtil.GetOdbcDrivers();
			foreach (string driver in drivers)
			{
				Driver.Add(NSObject.FromObject(driver));
			}
			#if MSSQL
			ServerType.Add(NSObject.FromObject("MSSQL"));
			#endif
			#if SSAS
			ServerType.Add(NSObject.FromObject("SQL Analysis Services"));
			#endif
			#if Oracle
			ServerType.Add(NSObject.FromObject("Oracle"));
			#endif
			#if PostgreSQL
			ServerType.Add(NSObject.FromObject("PostgreSQL"));
			#endif
			#if DB2
			ServerType.Add(NSObject.FromObject("DB2"));
			#endif
			#if Sybase_ASE
			ServerType.Add(NSObject.FromObject("SAP ASE"));
			#endif
			#if Sybase_Anywhere
			ServerType.Add(NSObject.FromObject("SAP Anywhere"));
			ServerType.Add(NSObject.FromObject("SAP IQ"));
			#endif
			#if Sybase_UltraLite
			ServerType.Add(NSObject.FromObject("SAP UltraLite"));
			#endif
			#if Sybase_Advantage
			ServerType.Add(NSObject.FromObject("SAP Advantage"));
			#endif
			#if Sap_Hana
			ServerType.Add(NSObject.FromObject("SAP HANA"));
			#endif
			#if MySQL
			ServerType.Add(NSObject.FromObject("MySQL"));
			#endif
			#if Firebird
			ServerType.Add(NSObject.FromObject("Firebird"));
			#endif
			#if Ingres
			ServerType.Add(NSObject.FromObject("Ingres"));
			#endif
			#if SQL_Azure
			ServerType.Add(NSObject.FromObject("SQL Azure"));
			#endif
			#if RedisCache
			ServerType.Add(NSObject.FromObject("Azure Redis Cache"));
			#endif
			#if Sqlite
			ServerType.Add(NSObject.FromObject("SQLite"));
			#endif
			#if Informix
			ServerType.Add(NSObject.FromObject("Informix"));
			#endif
			#if Interbase
			ServerType.Add(NSObject.FromObject("Interbase"));
			#endif
			#if Vertica
			ServerType.Add(NSObject.FromObject("Vertica"));
			#endif
			#if MongoDb
			ServerType.Add(NSObject.FromObject("MongoDb"));
			#endif
			#if Cassandra
			ServerType.Add(NSObject.FromObject("Cassandra"));
			#endif
			#if Redis
			ServerType.Add(NSObject.FromObject("Redis"));
            #endif
            #if Neo4j
            ServerType.Add(NSObject.FromObject("Neo4j"));
            #endif
            #if InfluxDb
            ServerType.Add(NSObject.FromObject("InfluxDb"));
            #endif
            SspiCheck();
			ServerType_Changed(this);
			ODBC_Toggled(this);
		}


		private void SspiCheck()
		{
			if (SSPI.State == NSCellStateValue.Off)
			{
				User.Enabled = true;
				Password.Enabled = true;
			}
			else
			{
				User.Enabled = false;
				Password.Enabled = false;
			}
			CombineConnectionString();
		}

		private void OdbcCheck()
		{
			if (ODBC.State == NSCellStateValue.On)
			{
				Driver.Enabled = true;
				btnDbList.Hidden = true;
			}
			else
			{
				Driver.Enabled = false;
				btnDbList.Hidden = false;
			}
			if (!string.IsNullOrWhiteSpace(ServerType.StringValue))
			{
				lblDatabase.StringValue = ServerType.StringValue.Contains("Informix") ? "Instance/DB" : "Database";
				CombineConnectionString();
			}
		}

		public static MetadataFinder CreateMetadataFinder(string serverType)
		{
			MetadataFinder finder;
			switch (serverType)
			{
			#if Sybase_ASE
			case "SAP ASE":
				finder = new SybaseMetadataFinder();
				break;
			#endif
				#if Sybase_Anywhere
			case "SAP Anywhere":
            case "SAP IQ":
				finder = new SybaseAnywhereMetadataFinder();
				break;
				#endif
				#if Sybase_Advantage
			case "SAP Advantage":
				finder = new SybaseAdvantageMetadataFinder();
				break;
				#endif
				#if Sap_Hana
			case "SAP HANA":
				finder = new SapHanaMetadataFinder();
				break;
				#endif
				#if Oracle
			case "Oracle":
				finder = new OracleMetadataFinder();
				break;
				#endif
				#if Firebird
			case "Firebird":
				finder = new FirebirdMetadataFinder();
				break;
				#endif
				#if Ingres
			case "Ingres":
				finder = new IngresMetadataFinder();
				break;
				#endif
				#if SQL_Azure
			case "SQL Azure":
				finder = new SqlAzureMetadataFinder();
				break;
				#endif
				#if RedisCache
			case "Azure Redis Cache":
				finder = new RedisMetadataFinder();
				break;
				#endif
				#if PostgreSQL
			case "PostgreSQL":
				finder = new PostgreSqlMetadataFinder();
				break;
				#endif
				#if MySQL
			case "MySQL":
				finder = new MySqlMetadataFinder();
				break;
				#endif
				#if MSSQL
			case "MSSQL":
				finder = new MsSqlMetadataFinder();
				break;
				#endif
				#if SSAS
			case "SQL Analysis Services":
				finder = new SsasMetadataFinder();
				break;
				#endif
				#if DB2
			case "DB2":
				finder = new Db2MetadataFinder();
				break;
				#endif
				#if Sqlite
			case "SQLite":
				finder = new SqliteMetadataFinder();
				break;
				#endif
				#if Informix
			case "Informix":
				finder = new InformixMetadataFinder();
				break;
				#endif
				#if Interbase
			case "Interbase":
				finder = new InterbaseMetadataFinder();
				break;
				#endif
				#if Sybase_UltraLite
			case "SAP UltraLite":
				finder = new UltraliteMetadataFinder();
				break;
				#endif
				#if Vertica
				case "Vertica":
					finder = new VerticaMetadataFinder();
					break;
				#endif
				#if MongoDb
				case "MongoDb":
					finder = new MongoDbMetadataFinder();
					break;
				#endif
				#if Cassandra
				case "Cassandra":
					finder = new CassandraMetadataFinder();
					break;
				#endif
				#if Redis
				case "Redis":
					finder = new RedisMetadataFinder();
					break;
                #endif
                #if Neo4j
                case "Neo4j":
                    finder = new Neo4jMetadataFinder();
                    break;
                #endif
                #if InfluxDb
                case "InfluxDb":
                    finder = new InfluxDbMetadataFinder();
                    break;
                #endif
                default:
				finder = null;
				break;
			}
			return finder;
		}

		private void CombineConnectionString()
		{
			MetadataFinder mf = CreateMetadataFinder(ServerType.StringValue);
			if (mf == null) return;
			string cstring =
				mf.MakeConnectionString(string.IsNullOrWhiteSpace(Driver.StringValue) ? "" : Driver.StringValue,
					Server.StringValue, Database.StringValue, User.StringValue, Password.StringValue, SSPI.State == NSCellStateValue.On,
					ODBC.State == NSCellStateValue.On ? ConnectionType.ODBC : ConnectionType.NET);

			if (string.IsNullOrWhiteSpace(cstring) && ODBC.State == NSCellStateValue.On)
			{
				cstring = "Connection string generation is not implemented for " + ServerType.StringValue +	" ODBC drivers";
			}

			cstring = HidePswd(cstring);

			ConnectionStringEntry.StringValue = cstring;
		}

		private static string HidePswd(string cstring)
		{
			int pwdIndex = cstring.IndexOf("Password=", StringComparison.OrdinalIgnoreCase);
			if (pwdIndex == -1)
			{
				pwdIndex = cstring.IndexOf("Pwd=", StringComparison.OrdinalIgnoreCase);
				if (pwdIndex != -1)
				{
					pwdIndex += 4;
				}
			}
			else
			{
				pwdIndex += 9;
			}
			if (pwdIndex != -1)
			{
				int scIndex = cstring.IndexOf(';', pwdIndex);
				string pswd = scIndex != -1 ? cstring.Substring(pwdIndex, scIndex - pwdIndex) : cstring.Substring(pwdIndex);
				if (!string.IsNullOrWhiteSpace(pswd))
				{
					cstring = cstring.Replace(pswd, new string('*', pswd.Length));
				}
			}
			return cstring;
		}

		public new ConfigWindow Window {
			get { return (ConfigWindow)base.Window; }
		}

		partial void ServerType_Changed (NSObject sender)
		{
			if (ServerType.SelectedIndex >= 0)
			{
				OdbcCheck();
				bool sspiAvailable = ServerType.StringValue.Contains("MSSQL") ||
					ServerType.StringValue.Contains("SQL Analysis Services") ||
					ServerType.StringValue.Contains("Oracle") ||
					ServerType.StringValue.Contains("PostgreSQL");
				Server.Enabled = !(ServerType.StringValue.Contains("SQLite"));
				SSPI.Enabled = sspiAvailable;
				if (!sspiAvailable) SSPI.State = NSCellStateValue.Off;
				CombineConnectionString();
			}
		}

		partial void Database_Changed (NSObject sender)
		{
			CombineConnectionString();
		}

		partial void Driver_Changed (NSObject sender)
		{
			CombineConnectionString();
		}

		partial void ODBC_Toggled (NSObject sender)
		{
			OdbcCheck();
		}

		partial void Password_Changed (NSObject sender)
		{
			CombineConnectionString();
		}

		partial void Server_Changed (NSObject sender)
		{
			CombineConnectionString();
		}

		partial void SSPI_Toggled (NSObject sender)
		{
			SspiCheck();
		}

		partial void User_Changed (NSObject sender)
		{
			CombineConnectionString();
		}

		partial void btnCancel_Click (NSObject sender)
		{
			DialogResult = 0;
			this.Window.PerformClose(sender);
		}

		partial void btnOk_Click(NSObject sender)
		{
			string str = "";
			if (ConnectionName.StringValue == "") str += "Connection name is empty" + Environment.NewLine;
			if (string.IsNullOrWhiteSpace(ServerType.StringValue)) str += "Database server type is empty" + Environment.NewLine;
			if (string.IsNullOrWhiteSpace(Server.StringValue) &&
				(ServerType.StringValue == null ||
					!(ServerType.StringValue.Contains("SQLite"))))
				str += "Server name or IP address is empty";
			if (str != "")
			{
				var md = new NSAlert();
				md.AlertStyle = NSAlertStyle.Warning;
				md.InformativeText = str;
				md.RunModal();
			}
			else
			{
				DialogResult = 1;
				this.Window.PerformClose(sender);
			}
			if (ListWindowController != null)
			{
				ListWindowController.ServerInfo = new ServerInfo(_ServerType, _Driver, _Server, _Database, _User, _Password, _SSPI.ToString(), _ConnectionType.ToString(), _ConnectionName);
			}
			if (WindowController != null)
			{
				WindowController.ServerInfo = new ServerInfo(_ServerType, _Driver, _Server, _Database, _User, _Password, _SSPI.ToString(), _ConnectionType.ToString(), _ConnectionName);
			}
		}

		partial void btnTest_Click(NSObject sender)
		{
			string str = "";
			if (string.IsNullOrWhiteSpace(ServerType.StringValue)) str += "Database server type is empty" + Environment.NewLine;
			if (string.IsNullOrWhiteSpace(Server.StringValue) &&
				(ServerType.StringValue == null ||
					!(ServerType.StringValue.Contains("SQLite"))))
				str += "Server name or IP address is empty";
			if (str != "")
			{
				var md = new NSAlert();
				md.AlertStyle = NSAlertStyle.Warning;
				md.InformativeText = str;
				md.RunModal();
				return;
			}
			if (!string.IsNullOrWhiteSpace(ServerType.StringValue))
			{
				MetadataFinder mf = CreateMetadataFinder(ServerType.StringValue);
				try
				{
					string cstring = mf.MakeConnectionString(
						string.IsNullOrWhiteSpace(Driver.StringValue) ? "" : Driver.StringValue,
						Server.StringValue, Database.StringValue, User.StringValue, Password.StringValue, SSPI.State == NSCellStateValue.On,
						ODBC.State == NSCellStateValue.On ? ConnectionType.ODBC : ConnectionType.NET);
					if (!string.IsNullOrWhiteSpace(cstring))
					{
						mf.EstablishConnection(cstring, ODBC.State == NSCellStateValue.On ? ConnectionType.ODBC : ConnectionType.NET, Database.StringValue);
					}
					else
					{
						var md = new NSAlert();
						md.AlertStyle = NSAlertStyle.Warning;
						md.InformativeText = "Connection string is empty";
						md.RunModal();
						return;
					}
					var mdSuccess = new NSAlert();
					mdSuccess.AlertStyle = NSAlertStyle.Informational;
					mdSuccess.InformativeText = "Connection successfull";
					mdSuccess.RunModal();
					return;
				}
				catch (Exception exception)
				{
					var md = new NSAlert();
					md.AlertStyle = NSAlertStyle.Critical;
					md.InformativeText = exception.GetExceptionMessages();
					md.RunModal();
				}
			}
		}

		partial void btnDbList_Click (NSObject sender)
		{
			string str = "";
			if (string.IsNullOrWhiteSpace(ServerType.StringValue)) str += "Database server type is empty" + Environment.NewLine;
			if (string.IsNullOrWhiteSpace(Server.StringValue) && (ServerType.StringValue == null || !ServerType.StringValue.Contains("Oracle")))
				str += "Server name or IP address is empty";
			if (str != "")
			{
				var md = new NSAlert();
				md.AlertStyle = NSAlertStyle.Warning;
				md.InformativeText = str;
				md.RunModal();
				return;
			}
			if (!string.IsNullOrWhiteSpace(ServerType.StringValue) && ServerType.StringValue != "Oracle")
			{
				MetadataFinder mf = CreateMetadataFinder(ServerType.StringValue);
				try
				{
					string cstring = mf.MakeConnectionString(
						string.IsNullOrWhiteSpace(Driver.StringValue) ? "" : Driver.StringValue,
						Server.StringValue, Database.StringValue, User.StringValue, Password.StringValue, SSPI.State == NSCellStateValue.On,
						ODBC.State == NSCellStateValue.On ? ConnectionType.ODBC : ConnectionType.NET);
					if (!string.IsNullOrWhiteSpace(cstring))
					{
						mf.EstablishConnection(cstring, ODBC.State == NSCellStateValue.On ? ConnectionType.ODBC : ConnectionType.NET, Database.StringValue);
					}
					else
					{
						var md = new NSAlert();
						md.AlertStyle = NSAlertStyle.Warning;
						md.InformativeText = "Connection string is empty";
						md.RunModal();
						return;
					}

					var list = mf.GetDatabaseList("Databases");
					foreach (string s in list)
					{
						Database.Add(NSObject.FromObject(s));
					}
				}
				catch (Exception exception)
				{
					var md = new NSAlert();
					md.AlertStyle = NSAlertStyle.Critical;
					md.InformativeText = exception.GetExceptionMessages();
					md.RunModal();
				}
			}
			else if(!string.IsNullOrWhiteSpace(ServerType.StringValue))
			{
				string[] text =
					new ConsoleWorker().StartConsoleProgram("tnsping",
						string.IsNullOrWhiteSpace(Server.StringValue)
						? "localhost"
						: Server.StringValue).Split('\n');
				foreach (string s in text)
				{
					if (s.Contains("sqlnet.ora"))
					{
						try
						{
							string tnsFile = s.Replace("sqlnet.ora", "tnsnames.ora");
							var dbs = OracleMetadataFinder.GetDatabases(tnsFile);
							foreach (string db in dbs)
							{
								Database.Add(NSObject.FromObject(db));
							}
						}
						catch (Exception exception)
						{
							var md = new NSAlert();
							md.AlertStyle = NSAlertStyle.Critical;
							md.InformativeText = exception.GetExceptionMessages();
							md.RunModal();
							return;
						}
					}
				}
			}
		}
	}
}
