﻿using System;

using AppKit;
using Foundation;

namespace SqlDynamiteM
{
	public partial class ViewController : NSViewController
	{
		public NSComboBox Search { get { return cmbSearch; } }
		public NSComboBox Connections { get { return cmbDb; } }
		public NSTextView DdlBox { get { return ddlBox; } }
		public NSTableView ObjectBox { get { return objectBox; } }
		public NSProgressIndicator ProgressBar { get { return progressBar; } }

		public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Do any additional setup after loading the view.
		}

		public override NSObject RepresentedObject {
			get {
				return base.RepresentedObject;
			}
			set {
				base.RepresentedObject = value;
				// Update the view, if already loaded.
			}
		}
	}
}
