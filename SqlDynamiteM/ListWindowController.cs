﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using AppKit;
using SqlDynamite.Common;
using SqlDynamite.Util;

namespace SqlDynamiteM
{
	public partial class ListWindowController : NSWindowController
	{
		internal static readonly string ConfigFile;

		internal ServerInfo ServerInfo { get; set; }

		private readonly DbListDataSource _dbListDataSource = new DbListDataSource ();

		static ListWindowController()
		{
			string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			if (!Directory.Exists(appdata + "/SQL Dynamite"))
			{
				Directory.CreateDirectory(appdata + "/SQL Dynamite");
			}
			ConfigFile = appdata + "/SQL Dynamite/SQL_Dynamite.xml";
		}

		public ListWindowController (IntPtr handle) : base (handle)
		{
		}

		[Export ("initWithCoder:")]
		public ListWindowController (NSCoder coder) : base (coder)
		{
		}

		public ListWindowController () : base ("ListWindow")
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			DbList.RemoveColumn (DbList.TableColumns() [1]);
			DbList.DoubleClick += DbList_DblClick;
			DbList.DataSource = _dbListDataSource;
			DbList.Delegate = new DbListDelegate ();
			FillConnections ();
		}

		public new ListWindow Window {
			get { return (ListWindow)base.Window; }
		}

		private void FillConnections()
		{
			(DbList.DataSource as DbListDataSource).Data.Clear ();
			var cns = XmlUtil.SelectConfig(ConfigFile).ToArray();
			int rowNumber = 0;
			var tableColumns = DbList.TableColumns ();
			foreach (Tuple<string, bool> cn in cns)
			{
				DbList.DataSource.SetObjectValue(DbList, new DbListDataSource.DbEntry { Name = new NSString(cn.Item1), IsDefault = cn.Item2 }, tableColumns[0], rowNumber);
				rowNumber++;
			}
			DbList.ReloadData ();
		}

		private void DbList_DblClick(object sender, EventArgs e)
		{
			btnEdit_Click (NSObject.FromObject(sender));
		}

		partial void btnAdd_Click (NSObject sender)
		{
			var configWindowController = new ConfigWindowController {ListWindowController = this};
			configWindowController.Window.WillClose += (object s, EventArgs e) => {
				if (NSApplication.SharedApplication.ModalWindow == configWindowController.Window)
				{
					NSApplication.SharedApplication.StopModalWithCode(configWindowController.DialogResult);
				}
			};
			var dialogResult = NSApplication.SharedApplication.RunModalForWindow(configWindowController.Window);
			if (dialogResult == 1)
			{
				XmlUtil.InsertConfig(ConfigFile, ServerInfo.Name, ServerInfo);
				FillConnections();
			}
		}

		partial void btnEdit_Click (NSObject sender)
		{
			var cnf = DbList.DataSource.GetObjectValue(DbList, DbList.TableColumns()[0], DbList.SelectedRow)?.ToString();
			if (string.IsNullOrWhiteSpace(cnf))
			{
				return;
			}			
			var configWindowController = new ConfigWindowController {ListWindowController = this};
			configWindowController.Window.WillClose += (object s, EventArgs e) => {
				if (NSApplication.SharedApplication.ModalWindow == configWindowController.Window)
				{
					NSApplication.SharedApplication.StopModalWithCode(configWindowController.DialogResult);
				}
			};
			var serverInfo = XmlUtil.SelectConfig(ListWindowController.ConfigFile, cnf);
			configWindowController._ServerType = serverInfo.ServerType;
			configWindowController._Server = serverInfo.Server;
			configWindowController._Driver = serverInfo.Driver;
			configWindowController._Database = serverInfo.Database;
			configWindowController._User = serverInfo.User;
			configWindowController._Password = serverInfo.Password;
			configWindowController._ConnectionName = serverInfo.Name;
			configWindowController._SSPI = serverInfo.SSPI == "True";
			configWindowController._ConnectionType = serverInfo.ConnectionType == "ODBC" ? ConnectionType.ODBC : ConnectionType.NET;
			var dialogResult = NSApplication.SharedApplication.RunModalForWindow(configWindowController.Window);
			if (dialogResult == 1)
			{
				XmlUtil.UpdateConfig(ConfigFile, cnf, ServerInfo.Name, ServerInfo);
				FillConnections();
			}
		}

		partial void btnMakeDefault_Click (NSObject sender)
		{
			var cnf = DbList.DataSource.GetObjectValue(DbList, DbList.TableColumns()[0], DbList.SelectedRow)?.ToString();
			if (!string.IsNullOrWhiteSpace(cnf))
			{
				XmlUtil.MakeDefaultConfig(ConfigFile, cnf);
				FillConnections();
			}
		}

		partial void btnRemove_Click (NSObject sender)
		{
			var cnf = DbList.DataSource.GetObjectValue(DbList, DbList.TableColumns()[0], DbList.SelectedRow)?.ToString();
			var alert = new NSAlert();
			alert.AlertStyle = NSAlertStyle.Warning;
			alert.InformativeText = "Do you want to delete this entry?";
			alert.MessageText  ="Confirmation";
			alert.AddButton("Ok");
			alert.AddButton("Cancel");
			var result = alert.RunModal();
			if (result == 1000)
			{
				if (!string.IsNullOrWhiteSpace(cnf))
				{                
					XmlUtil.DeleteConfig(ConfigFile, cnf);
					FillConnections();
				}
			}
		}
	}
}
