﻿using System;

using Foundation;
using AppKit;

namespace SqlDynamiteM
{
	public partial class ListWindow : NSWindow
	{
		public ListWindow (IntPtr handle) : base (handle)
		{
		}

		[Export ("initWithCoder:")]
		public ListWindow (NSCoder coder) : base (coder)
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
		}
	}
}
