﻿using System;
using AppKit;
using SqlDynamite.Util;
using SqlDynamite.Common;

namespace SqlDynamiteM
{
	public class ObjectListDelegate : NSTableViewDelegate
	{
		#region Constants 
		private const string CellIdentifier = "ObjectCell";
		#endregion

		#region Public Properties
		public NSTableView ObjectBox { get; set; }
		public NSTextView DdlBox { get; set; }
		public NSTableColumn[] TableColumns { get; set; }
		public ServerInfo ServerInfo { get; set; }
		#endregion

		#region Override Methods
		public override NSView GetViewForItem(NSTableView tableView, NSTableColumn tableColumn, nint row)
		{
			// This pattern allows you reuse existing views when they are no-longer in use.
			// If the returned view is null, you instance up a new view
			// If a non-null view is returned, you modify it enough to reflect the new data
			NSTextField view = (NSTextField)tableView.MakeView(CellIdentifier, this);
			if (view == null)
			{
				view = new NSTextField();
				view.Identifier = CellIdentifier;
				view.BackgroundColor = NSColor.Clear;
				view.Bordered = false;
				view.Selectable = false;
				view.Editable = false;
			}

			return view;
		}

		public override void SelectionDidChange(Foundation.NSNotification notification)
		{
			if (ObjectBox.SelectedRow > 0)
			{
				MetadataFinder finder = ConfigWindowController.CreateMetadataFinder(ServerInfo.ServerType);
				if (finder == null) return;
				DdlBox.Value = "";
				string objectName = ObjectBox.DataSource.GetObjectValue(ObjectBox, TableColumns[0], ObjectBox.SelectedRow).ToString();
				string typeStr = ObjectBox.DataSource.GetObjectValue(ObjectBox, TableColumns[1], ObjectBox.SelectedRow).ToString();
				ObjectType type = MetadataFinder.String2ObjectType(typeStr);
				var connectionType = ServerInfo.ConnectionType == "ODBC" ? ConnectionType.ODBC : ConnectionType.NET;
				string cstring = finder.MakeConnectionString(ServerInfo.Driver, ServerInfo.Server, ServerInfo.Database, ServerInfo.User, ServerInfo.Password, ServerInfo.SSPI == "True", connectionType);
				if (!string.IsNullOrWhiteSpace(cstring))
				{
					finder.EstablishConnection(cstring, connectionType, ServerInfo.Database);
				}
				else
				{
					var md = new NSAlert() { InformativeText = "Connection string is empty", MessageText = "Error" };
					md.RunModal();
					return;
				}

				try
				{
					string content = finder.RetrieveDefinition(objectName, type, null);
					DdlBox.Value = content;
				}
				catch (Exception exc)
				{
					var md = new NSAlert() { InformativeText = exc.GetExceptionMessages(), MessageText = "Error" };
					md.RunModal();
				}
			}
		}
		#endregion
	}
}

