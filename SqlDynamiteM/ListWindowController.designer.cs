// WARNING
//
// This file has been generated automatically by Xamarin Studio Community to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace SqlDynamiteM
{
	[Register ("ListWindowController")]
	partial class ListWindowController
	{
		[Outlet]
		AppKit.NSTableView DbList { get; set; }

		[Action ("btnAdd_Click:")]
		partial void btnAdd_Click (Foundation.NSObject sender);

		[Action ("btnEdit_Click:")]
		partial void btnEdit_Click (Foundation.NSObject sender);

		[Action ("btnMakeDefault_Click:")]
		partial void btnMakeDefault_Click (Foundation.NSObject sender);

		[Action ("btnRemove_Click:")]
		partial void btnRemove_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (DbList != null) {
				DbList.Dispose ();
				DbList = null;
			}
		}
	}
}
