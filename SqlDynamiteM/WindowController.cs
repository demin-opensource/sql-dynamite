﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
//using System.Threading.Tasks;
using Microsoft.FSharp.Collections;
using AppKit;
using Foundation;
using SqlDynamite.Common;
using SqlDynamite.Util;
using System.IO;

namespace SqlDynamiteM
{
	[Register("WindowController")]
	public partial class WindowController : NSWindowController
	{
		private static readonly string HistoryFile;
		private static readonly string LogFile;
		private List<string> _words;
		private List<Tuple<string, ObjectType, string>> _objects;
		private string _serverType;
		private string _driver;
		private string _server;
		private string _database;
		private string _user;
		private string _password;
		private bool _sspi;
		private ConnectionType _connectionType;
		//private Task _task;
		private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
		//private CancellationToken _cancellationToken;
		private bool _tableCheck = true;
		private bool _pkCheck = true;
		private bool _fkCheck = true;
		private bool _ixCheck = true;
		private bool _spCheck = true;
		private bool _fnCheck = true;
		private bool _viewCheck = true;
		private bool _trCheck = true;
		private bool _packageCheck = true;
		private bool _sequenceCheck = true;
		private bool _synonymCheck = true;
		private bool _typeCheck = true;
		private bool _jobCheck = true;
		private bool _reportCheck = true;
        private bool _serviceBrokerCheck = true;
        private bool _computedColumnsCheck = true;
		private bool _searchInNames = true;
		private bool _searchInDef = true;

		private bool _matchCase;
		private bool _syntaxHighlighting;

		private NSTextView ddlBox;
		private readonly List<Tuple<string, string>> _items = new List<Tuple<string, string>> ();
		private NSProgressIndicator progressBar;
		private NSTableView objectBox;
		private NSTableColumn[] tableColumns;
		private ObjectListDataSource _objectListDataSource = new ObjectListDataSource();

		public ServerInfo ServerInfo { get; set; }

		static WindowController()
		{
			string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			if (!Directory.Exists(appdata + "/SQL Dynamite"))
			{
				Directory.CreateDirectory(appdata + "/SQL Dynamite");
			}
			HistoryFile = appdata + "/SQL Dynamite/SQL_Dynamite_HISTORY.txt";
			LogFile = appdata + "/SQL Dynamite/SQL_Dynamite_LOG.txt";
		}

		public WindowController ()
		{
		}

		public WindowController(IntPtr handle) : base(handle)
		{
		}

		public WindowController(NSObjectFlag t) : base(t)
		{
		}

		public WindowController(string title) : base(title)
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			btnStop.Active = false;
			_words = new List<string>();
			var viewController = this.Window.ContentViewController as ViewController;
			if (!File.Exists(HistoryFile))
			{
				File.CreateText(HistoryFile);
			}
			else
			{
				_words.AddRange(File.ReadAllLines(HistoryFile));
				_words.Reverse();
				foreach (string word in _words)
				{
					
					viewController.Search.Add(NSObject.FromObject(word));
				}
			}
			FillConnections();
			if (viewController.Connections.VisibleItems > 0)
			{
				IEnumerable<Tuple<string, bool>> confs = XmlUtil.SelectConfig(ListWindowController.ConfigFile);
				foreach (Tuple<string, bool> conf in confs)
				{
					if (conf.Item2)
					{
						viewController.Connections.StringValue = conf.Item1;
						break;
					}
				}
				if (viewController.Connections.StringValue == "")
				{
					viewController.Connections.DeselectItem(viewController.Connections.SelectedIndex);
				}
			}
			else
			{
				var configWindowController = new ConfigWindowController {WindowController = this};
				configWindowController.Window.WillClose += (object s, EventArgs e) => {
					if (NSApplication.SharedApplication.ModalWindow == configWindowController.Window)
					{
						NSApplication.SharedApplication.StopModalWithCode(configWindowController.DialogResult);
					}
				};
				var dialogResult = NSApplication.SharedApplication.RunModalForWindow(configWindowController.Window);
				if (dialogResult == 1)
				{
					XmlUtil.InsertConfig(ListWindowController.ConfigFile, ServerInfo.Name, ServerInfo);
					FillConnections();
				}
			}

			ddlBox = (this.ContentViewController as ViewController).DdlBox;

			objectBox = (this.ContentViewController as ViewController).ObjectBox;
			progressBar = (this.ContentViewController as ViewController).ProgressBar;
			progressBar.MinValue = 0;
			progressBar.MaxValue = 100;
			Connections_SelectionChanged(this, null);

			(this.ContentViewController as ViewController).Connections.SelectionChanged += Connections_SelectionChanged;
			objectBox.DataSource = _objectListDataSource;
			objectBox.Delegate = new ObjectListDelegate();
			tableColumns = objectBox.TableColumns();
			tableColumns[0].Identifier = "ObjectName";
			tableColumns[1].Identifier = "ObjectType";
			((ObjectListDelegate)objectBox.Delegate).ObjectBox = objectBox;
			((ObjectListDelegate)objectBox.Delegate).DdlBox = ddlBox;
			((ObjectListDelegate)objectBox.Delegate).TableColumns = tableColumns;
		}

		private void FillListBox()
		{
			List<Tuple<string, ObjectType, string>> list = new List<Tuple<string, ObjectType, string>>();

			foreach (Tuple<string, ObjectType, string> pair in _objects)
			{
				Tuple<string, ObjectType, string> pair1 = pair;
				bool keyFound = _items.Cast<object[]>().Any(item => item[0].ToString() == pair1.Item1);

				if (!keyFound)
				{
					list.Add(new Tuple<string, ObjectType, string>(pair.Item1, pair.Item2, _database));
				}
			}
			list.Sort(new TupleComparer());
			int rowNumber = 0;
			foreach (Tuple<string, ObjectType, string> pair in list)
			{
				_items.Add(new Tuple<string, string> (pair.Item1, pair.Item2.ToString()));
				objectBox.DataSource.SetObjectValue(objectBox, NSObject.FromObject(pair.Item1), tableColumns[0], rowNumber);
				objectBox.DataSource.SetObjectValue(objectBox, NSObject.FromObject(pair.Item2.ToString()), tableColumns[1], rowNumber);
				rowNumber++;
			}
			objectBox.ReloadData();
		}

		private void SearchObjects(string query, string searchStr, MetadataFinder finder, List<ObjectType> types,
			bool[] boxes)
		{
			//_cancellationTokenSource = new CancellationTokenSource();
			//_cancellationToken = _cancellationTokenSource.Token;

			//if (_task != null && _task.Status == TaskStatus.Running)
			//{
			//	_cancellationTokenSource.Cancel();
			//}

			FSharpList<Tuple<string, ObjectType, string>> objects = null;

			objects = finder.SearchInNames(boxes, query, _cancellationTokenSource);
			progressBar.DoubleValue = 25;
			objects = MetadataFinder.JoinLists(objects, finder.SearchInDefinitionsByName(boxes, searchStr, _matchCase, _cancellationTokenSource));
			progressBar.DoubleValue = 50;
			objects = MetadataFinder.JoinLists(objects, finder.SearchInDefinitionsByContent(boxes, searchStr, types.Aggregate(FSharpList<ObjectType>.Empty, (current, type) => new FSharpList<ObjectType>(type, current)), _matchCase, _cancellationTokenSource));
			progressBar.DoubleValue = 100;
			_objects = new List<Tuple<string, ObjectType, string>>(objects);
			CompleteSearch();
		}

		private void CompleteSearch()
		{
			FillListBox();
			btnStop.Active = false;
			btnRun.Active = true;
			progressBar.DoubleValue = 0;
			((ObjectListDelegate)objectBox.Delegate).ServerInfo = new ServerInfo(_serverType, _driver, _server, _database, _user, _password, _sspi.ToString(), _connectionType.ToString(), (this.ContentViewController as ViewController).Connections.StringValue);
		}

		partial void btnRun_Click (NSObject sender)
		{
			MetadataFinder finder = ConfigWindowController.CreateMetadataFinder(_serverType);
			if (finder == null) return;
			var types = new List<ObjectType>();
			if (_spCheck) types.Add(ObjectType.PROCEDURE);
			if (_fnCheck) types.Add(ObjectType.FUNCTION);
			if (_tableCheck) types.Add(ObjectType.TABLE);
			if (_viewCheck) types.Add(ObjectType.VIEW);
			if (_trCheck) types.Add(ObjectType.TRIGGER);
			if (_pkCheck) types.Add(ObjectType.PRIMARY_KEY);
			if (_fkCheck) types.Add(ObjectType.FOREIGN_KEY);
			if (_ixCheck) types.Add(ObjectType.INDEX);
			if (_packageCheck) types.Add(ObjectType.PACKAGE);
			if (_sequenceCheck) types.Add(ObjectType.SEQUENCE);
			if (_synonymCheck) types.Add(ObjectType.SYNONYM);
			if (_typeCheck) types.Add(ObjectType.TYPE);
			if (_jobCheck) types.Add(ObjectType.JOB);
			if (_reportCheck) types.Add(ObjectType.REPORT);
            if (_serviceBrokerCheck) types.Add(ObjectType.SERVICE_QUEUE);
            if (_computedColumnsCheck) types.Add(ObjectType.COMPUTED_COLUMN);
			if (types.Count == 0) return;
			if (_searchInNames == false && _searchInDef == false) return;
			var cmbSearch = (this.ContentViewController as ViewController).Search;
			if (!string.IsNullOrWhiteSpace(cmbSearch.StringValue)
				&& !_words.Contains(cmbSearch.StringValue.Trim()))
			{
				string activeText = cmbSearch.StringValue.Trim();
				_words.Add(activeText);
				cmbSearch.Insert(NSObject.FromObject(activeText), 0);
				File.AppendAllText(HistoryFile, activeText + Environment.NewLine);
			}
			_items.Clear();
			try
			{
				string cstring = finder.MakeConnectionString(_driver, _server, _database, _user, _password, _sspi, _connectionType);
				if (!string.IsNullOrWhiteSpace(cstring))
				{
					finder.EstablishConnection(cstring, _connectionType, _database);
				}
				else
				{
					var md = new NSAlert() { InformativeText = "Connection string is empty", MessageText = "Error" };
					md.RunModal();
					return;
				}
			}
			catch (Exception exception)
			{
				File.AppendAllText(LogFile,
					string.Format("{0}: {1}{2}", DateTime.Now, exception.GetExceptionMessages(), Environment.NewLine));
				var md = new NSAlert() { InformativeText = exception.GetExceptionMessages(), MessageText = "Error while searching in names" };
				md.RunModal();
				return;
			}
			_objects = new List<Tuple<string, ObjectType, string>>();
			bool[] boxes = 
			{
				_tableCheck,
				_pkCheck,
				_fkCheck,
				_ixCheck,
				_spCheck,
				_fnCheck,
				_viewCheck,
				_trCheck,
				_sequenceCheck,
				_packageCheck,
				_synonymCheck,
				_typeCheck,
				_jobCheck,
				_reportCheck,
                _serviceBrokerCheck,
                _computedColumnsCheck,
				_searchInNames,
				_searchInDef
			};
			ddlBox.Value = "";
			btnRun.Active = false;
			btnStop.Active = true;
			string query = _searchInNames
				? finder.GenerateNameScript(cmbSearch.StringValue.Trim(), types.Aggregate(FSharpList<ObjectType>.Empty, (current, type) => new FSharpList<ObjectType>(type, current)), _matchCase)
				: null;
			SearchObjects(query, cmbSearch.StringValue.Trim().Replace("'", "''"), finder, types, boxes);
		}

		partial void btnStop_Click (NSObject sender)
		{
			_cancellationTokenSource.Cancel();
		}

		partial void btnManage_Click (NSObject sender)
		{
			var listWindowController = new ListWindowController();
			var listWindow = new ListWindow(this.Handle);
			listWindowController.ShowWindow(listWindow);
		}

		partial void btnExit_Click(NSObject sender)
		{
			NSApplication.SharedApplication.Terminate(this);
		}

		partial void btnDrivers_Click (NSObject sender)
		{
			var drivers = "";
			foreach (var tuple in MetadataFinderHelper.ProvidersList.Distinct())
			{
				drivers += $"{tuple.Item1}: {tuple.Item2}{Environment.NewLine}{Environment.NewLine}";
			}
			var md = new NSAlert { InformativeText = drivers, MessageText = "Installed .NET Data Providers" };
			md.RunModal();
		}

		private void Connections_SelectionChanged(object sender, EventArgs e)
		{
			var conns = (this.ContentViewController as ViewController).Connections;
			var str = conns.SelectedValue?.ToString() ?? conns.StringValue;
			if (!string.IsNullOrWhiteSpace(str))
			{
				ServerInfo crs =
					XmlUtil.SelectConfig(ListWindowController.ConfigFile, str);
				if (crs != null)
				{
					_serverType = crs.ServerType;
					_driver = crs.Driver;
					_server = crs.Server;
					_database = crs.Database;
					_user = crs.User;
					_password = crs.Password;
					_sspi = bool.Parse(crs.SSPI);
					_connectionType = crs.ConnectionType == "ODBC"
						? ConnectionType.ODBC
						: crs.ConnectionType == "OLEDB" ? ConnectionType.OLEDB : ConnectionType.NET;
				}
			}
		}

		private void FillConnections()
		{
			var conns = (this.ContentViewController as ViewController).Connections;
			string currentConnection = conns.StringValue;
			IEnumerable<Tuple<string, bool>> cns = XmlUtil.SelectConfig(ListWindowController.ConfigFile);
			foreach (Tuple<string, bool> cn in cns)
			{
				conns.Add(NSObject.FromObject(cn.Item1));
			}
			conns.StringValue = currentConnection;
		}

		partial void btnCaseSensitive_Click(NSObject sender)
		{
			_matchCase = !_matchCase;
		}

		partial void btnHighlight_Click(NSObject sender)
		{
			_syntaxHighlighting = !_syntaxHighlighting;
		}

	}
}

