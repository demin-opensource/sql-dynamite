// WARNING
//
// This file has been generated automatically by Xamarin Studio Community to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace SqlDynamiteM
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSComboBox cmbDb { get; set; }

		[Outlet]
		AppKit.NSComboBox cmbSearch { get; set; }

		[Outlet]
		AppKit.NSTextView ddlBox { get; set; }

		[Outlet]
		AppKit.NSTableView objectBox { get; set; }

		[Outlet]
		AppKit.NSProgressIndicator progressBar { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (cmbDb != null) {
				cmbDb.Dispose ();
				cmbDb = null;
			}

			if (cmbSearch != null) {
				cmbSearch.Dispose ();
				cmbSearch = null;
			}

			if (ddlBox != null) {
				ddlBox.Dispose ();
				ddlBox = null;
			}

			if (objectBox != null) {
				objectBox.Dispose ();
				objectBox = null;
			}

			if (progressBar != null) {
				progressBar.Dispose ();
				progressBar = null;
			}
		}
	}
}
