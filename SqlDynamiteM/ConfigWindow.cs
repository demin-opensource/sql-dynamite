﻿using System;

using Foundation;
using AppKit;

namespace SqlDynamiteM
{
	public partial class ConfigWindow : NSWindow
	{
		public ConfigWindow (IntPtr handle) : base (handle)
		{
		}

		[Export ("initWithCoder:")]
		public ConfigWindow (NSCoder coder) : base (coder)
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
		}
	}
}
