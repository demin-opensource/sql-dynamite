﻿using System;
using AppKit;

namespace SqlDynamiteM
{
	public class DbListDelegate : NSTableViewDelegate
	{
		#region Constants 
		private const string CellIdentifier = "DbCell";
		#endregion

		#region Override Methods
		public override NSView GetViewForItem (NSTableView tableView, NSTableColumn tableColumn, nint row)
		{
			// This pattern allows you reuse existing views when they are no-longer in use.
			// If the returned view is null, you instance up a new view
			// If a non-null view is returned, you modify it enough to reflect the new data
			NSTextField view = (NSTextField)tableView.MakeView (CellIdentifier, this);
			if (view == null) {
				var entry = ((DbListDataSource)tableView.DataSource).Data[(int)row] as DbListDataSource.DbEntry;
				view = new NSTextField ();
				view.Identifier = CellIdentifier;
				view.BackgroundColor = entry.IsDefault ? NSColor.Purple : NSColor.Clear;
				view.Bordered = false;
				view.Selectable = false;
				view.Editable = false;
			}

			return view;
		}
		#endregion
	}
}

