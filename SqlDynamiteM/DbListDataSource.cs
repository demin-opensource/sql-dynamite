﻿using System;
using System.Collections.Generic;
using AppKit;
using Foundation;

namespace SqlDynamiteM
{
	public class DbListDataSource : NSTableViewDataSource
	{
		public class DbEntry : NSObject
		{
			public NSString Name { get; set; }
			public bool IsDefault { get; set; }
		}

		private IList<DbEntry> _data;

		public IList<DbEntry> Data { get { return _data; } }

		public DbListDataSource ()
		{
			_data = new List<DbEntry> ();
		}

		public override nint GetRowCount (NSTableView tableView)
		{
			return _data.Count;
		}

		public override NSObject GetObjectValue (NSTableView tableView, NSTableColumn tableColumn, nint row)
		{
			return row >= 0 ? _data [(int)row].Name : null;
		}

		public override void SetObjectValue (NSTableView tableView, NSObject theObject, NSTableColumn tableColumn, nint row)
		{
			var entry = theObject as DbEntry;
			if (row >= _data.Count)
			{
				_data.Add(entry);
			}
			else
			{
				_data [(int)row] = entry;
			}
		}
	}
}

