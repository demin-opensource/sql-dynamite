// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace SqlDynamiteM
{
	partial class WindowController
	{
		[Outlet]
		SqlDynamiteM.ActivatableItem btnCaseSensitive { get; set; }

		[Outlet]
		SqlDynamiteM.ActivatableItem btnHighlight { get; set; }

		[Outlet]
		SqlDynamiteM.ActivatableItem btnRun { get; set; }

		[Outlet]
		SqlDynamiteM.ActivatableItem btnStop { get; set; }

		[Outlet]
		AppKit.NSToolbar MainToolbar { get; set; }

		[Action ("btnCaseSensitive_Click:")]
		partial void btnCaseSensitive_Click (Foundation.NSObject sender);

		[Action ("btnDrivers_Click:")]
		partial void btnDrivers_Click (Foundation.NSObject sender);

		[Action ("btnExit_Click:")]
		partial void btnExit_Click (Foundation.NSObject sender);

		[Action ("btnHighlight_Click:")]
		partial void btnHighlight_Click (Foundation.NSObject sender);

		[Action ("btnManage_Click:")]
		partial void btnManage_Click (Foundation.NSObject sender);

		[Action ("btnPreferences_Click:")]
		partial void btnPreferences_Click (Foundation.NSObject sender);

		[Action ("btnRun_Click:")]
		partial void btnRun_Click (Foundation.NSObject sender);

		[Action ("btnStop_Click:")]
		partial void btnStop_Click (Foundation.NSObject sender);

		[Action ("btnWordWrap_Click:")]
		partial void btnWordWrap_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnRun != null) {
				btnRun.Dispose ();
				btnRun = null;
			}

			if (btnStop != null) {
				btnStop.Dispose ();
				btnStop = null;
			}

			if (MainToolbar != null) {
				MainToolbar.Dispose ();
				MainToolbar = null;
			}

			if (btnHighlight != null) {
				btnHighlight.Dispose ();
				btnHighlight = null;
			}

			if (btnCaseSensitive != null) {
				btnCaseSensitive.Dispose ();
				btnCaseSensitive = null;
			}
		}
	}
}
