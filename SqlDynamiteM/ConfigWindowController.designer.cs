// WARNING
//
// This file has been generated automatically by Xamarin Studio Community to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace SqlDynamiteM
{
	[Register ("ConfigWindowController")]
	partial class ConfigWindowController
	{
		[Outlet]
		AppKit.NSButton btnDbList { get; set; }

		[Outlet]
		AppKit.NSTextField ConnectionName { get; set; }

		[Outlet]
		AppKit.NSTextField ConnectionStringEntry { get; set; }

		[Outlet]
		AppKit.NSComboBox Database { get; set; }

		[Outlet]
		AppKit.NSComboBox Driver { get; set; }

		[Outlet]
		AppKit.NSTextField lblDatabase { get; set; }

		[Outlet]
		AppKit.NSTextField lblServer { get; set; }

		[Outlet]
		AppKit.NSButton ODBC { get; set; }

		[Outlet]
		AppKit.NSSecureTextField Password { get; set; }

		[Outlet]
		AppKit.NSTextField Server { get; set; }

		[Outlet]
		AppKit.NSComboBox ServerType { get; set; }

		[Outlet]
		AppKit.NSButton SSPI { get; set; }

		[Outlet]
		AppKit.NSTextField User { get; set; }

		[Action ("btnCancel_Click:")]
		partial void btnCancel_Click (Foundation.NSObject sender);

		[Action ("btnDbList_Click:")]
		partial void btnDbList_Click (Foundation.NSObject sender);

		[Action ("btnOk_Click:")]
		partial void btnOk_Click (Foundation.NSObject sender);

		[Action ("btnTest_Click:")]
		partial void btnTest_Click (Foundation.NSObject sender);

		[Action ("Database_Changed:")]
		partial void Database_Changed (Foundation.NSObject sender);

		[Action ("Driver_Changed:")]
		partial void Driver_Changed (Foundation.NSObject sender);

		[Action ("ODBC_Toggled:")]
		partial void ODBC_Toggled (Foundation.NSObject sender);

		[Action ("Password_Changed:")]
		partial void Password_Changed (Foundation.NSObject sender);

		[Action ("Server_Changed:")]
		partial void Server_Changed (Foundation.NSObject sender);

		[Action ("ServerType_Changed:")]
		partial void ServerType_Changed (Foundation.NSObject sender);

		[Action ("SSPI_Toggled:")]
		partial void SSPI_Toggled (Foundation.NSObject sender);

		[Action ("User_Changed:")]
		partial void User_Changed (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (lblServer != null) {
				lblServer.Dispose ();
				lblServer = null;
			}

			if (lblDatabase != null) {
				lblDatabase.Dispose ();
				lblDatabase = null;
			}

			if (btnDbList != null) {
				btnDbList.Dispose ();
				btnDbList = null;
			}

			if (ConnectionName != null) {
				ConnectionName.Dispose ();
				ConnectionName = null;
			}

			if (ConnectionStringEntry != null) {
				ConnectionStringEntry.Dispose ();
				ConnectionStringEntry = null;
			}

			if (Database != null) {
				Database.Dispose ();
				Database = null;
			}

			if (Driver != null) {
				Driver.Dispose ();
				Driver = null;
			}

			if (ODBC != null) {
				ODBC.Dispose ();
				ODBC = null;
			}

			if (Password != null) {
				Password.Dispose ();
				Password = null;
			}

			if (Server != null) {
				Server.Dispose ();
				Server = null;
			}

			if (ServerType != null) {
				ServerType.Dispose ();
				ServerType = null;
			}

			if (SSPI != null) {
				SSPI.Dispose ();
				SSPI = null;
			}

			if (User != null) {
				User.Dispose ();
				User = null;
			}
		}
	}
}
