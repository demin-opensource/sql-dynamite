﻿using System;
using AppKit;
using System.Collections.Generic;
using Foundation;

namespace SqlDynamiteM
{
	public class ObjectListDataSource : NSTableViewDataSource
	{
		public class ObjectEntry
		{
			public NSString Name { get; set; }
			public NSString Type { get; set; }
		}

		private IList<ObjectEntry> _data;

		public IList<ObjectEntry> Data { get { return _data; } }

		public ObjectListDataSource ()
		{
			_data = new List<ObjectEntry> ();
		}

		public override nint GetRowCount (NSTableView tableView)
		{
			return _data.Count;
		}

		public override NSObject GetObjectValue (NSTableView tableView, NSTableColumn tableColumn, nint row)
		{
			return row >= 0 ? (tableColumn.Identifier == "ObjectName" ? _data [(int)row].Name : _data[(int)row].Type) : null;
		}

		public override void SetObjectValue (NSTableView tableView, NSObject theObject, NSTableColumn tableColumn, nint row)
		{
			if (row >= _data.Count)
			{
				if (tableColumn.Identifier == "ObjectName")
				{
					_data.Add(new ObjectEntry { Name = new NSString(theObject.ToString()) });
				}
				else
				{
					_data.Add(new ObjectEntry { Type = new NSString(theObject.ToString()) });
				}
			}
			else
			{
				if (tableColumn.Identifier == "ObjectName")
				{
					_data[(int)row].Name = new NSString(theObject.ToString());
				}
				else
				{
					_data[(int)row].Type = new NSString(theObject.ToString());
				}
			}
		}
	}
}

