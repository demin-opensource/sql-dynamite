﻿using System.Drawing;
using Gtk;

namespace SqlDynamiteX
{
    public static class GtkExtensions
    {
        public static object GetSelectedItem(this TreeView view, int index)
        {
            if (view.Selection.GetSelected(out var model, out var iter))
            {
                return model.GetValue(iter, index);
            }
            return null;
        }

        public static void SetComboBoxText(this ComboBox box, string value)
        {
            int count = box.Model.IterNChildren();
            for (int index = 0; index < count; index++)
            {
                box.Model.IterNthChild(out var iter, index);
                string str = box.Model.GetValue(iter, 0).ToString();
                if (str == value)
                {
                    box.Active = index;
                    break;
                }
            }
        }

        public static bool Equal(this Pango.Color c1, Pango.Color c2)
        {
            return c1.Red == c2.Red && c1.Green == c2.Green && c1.Blue == c2.Blue;
        }

        public static string ToColorString(this Pango.Color color)
        {
            var str = color.ToString();
            var cstr = "#" + str.Substring(3, 2) + str.Substring(7, 2) + str.Substring(11, 2);
            return new ColorConverter().ConvertFromString(cstr).ToString().Replace("Color [", "").Replace("]", "");
        }
    }
}
