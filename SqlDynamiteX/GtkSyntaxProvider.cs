﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pango;
using SqlDynamite.Util;

namespace SqlDynamiteX
{
	public class GtkSyntaxProvider : BaseSyntaxProvider
	{
		public readonly Color KeywordColor = new Color {Red = 0, Green = 0, Blue = 255};
		public readonly Color FunctionColor = new Color {Red = 255, Green = 0, Blue = 255};
		public readonly Color FunctionNoAnsiColor = new Color {Red = 0xFA, Green = 128, Blue = 0x72};
		public readonly Color TypeColor = new Color {Red = 128, Green = 0, Blue = 128};
	    public readonly Color SearchColor = new Color {Red = 255, Green = 0, Blue = 0};
        public readonly Color SearchBackgroundColor = new Color {Red = 255, Green = 255, Blue = 0xE0};

	    private readonly Color[] ColorList;

		private readonly Dictionary<string, Color> TagsList = new Dictionary<string, Color>();

	    public GtkSyntaxProvider()
	    {
	        ColorList = new[]
	        {
	            KeywordColor, FunctionColor, FunctionNoAnsiColor, TypeColor, SearchColor
	        };
	    }

		public void LoadWords(string words, Color color)
		{
			string[] strs = words.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);

			for (int index = 0; index < strs.Length; index++)
			{
				strs[index] = strs[index].Replace("\r", "");
			}

            foreach (string str in strs)
			{
				if (!TagsList.ContainsKey(str.ToLower()) && !TagsList.ContainsKey(str.ToUpper()))
				{
					TagsList[str.ToLower()] = color;
					TagsList[str.ToUpper()] = color;
				}
			}
		}

		public void UnloadWords()
		{
			var indexesToDelete =
				TagsList.Where(
					pair =>
					pair.Value.Equal(FunctionNoAnsiColor)).
				ToList();
			foreach (KeyValuePair<string, Color> pair in indexesToDelete)
			{
				TagsList.Remove(pair.Key);
			}
		}

		public Dictionary<string, Color> Tags => TagsList;

		public IEnumerable<Color> Colors => ColorList;

		public bool IsKnownTag(string tag)
		{
			return TagsList.ContainsKey(tag);
		}
	}
}