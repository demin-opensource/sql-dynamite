﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SqlDynamiteX
{
	public static class SqlSyntaxProvider
	{
		public static readonly Color KeywordColor = Color.Blue;
		public static readonly Color FunctionColor = Color.Magenta;
		public static readonly Color FunctionNoAnsiColor = Color.Salmon;
		public static readonly Color TypeColor = Color.Purple;
		public static readonly Color SearchColor = Color.Red;
		public static readonly Color SearchBackgroundColor = Color.LightYellow;

		private static readonly Color[] ColorList =
		{
			KeywordColor, FunctionColor, FunctionNoAnsiColor, TypeColor, SearchColor
		};

		public static readonly char[] Separators = { ' ', '\n', '\r', '\t', ',', '.', '+', '=', '<', '>', '(', ')', '[', ']', ';', ':', '?', '!' };

		private static readonly Dictionary<string, Color> TagsList = new Dictionary<string, Color>();

		private static readonly string[] csKeywords =
		{
			"abstract", "as", "base", "break", "case", "catch", "checked", "class", "const", "continue", "default",
			"delegate", "do", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed", "for",
			"foreach", "goto", "if", "implicit", "in", "interface", "internal", "is", "lock", "namespace", "new",
			"null", "operator", "out", "override", "params", "private", "protected", "public", "readonly",
			"ref", "return", "sealed", "sizeof", "stackalloc", "static", "struct", "switch", "this", "throw", "true",
			"try", "typeof", "unchecked", "unsafe", "using", "virtual", "volatile", "while", "add", "alias",
			"ascending", "async", "await", "descending", "dynamic", "from", "get", "global", "group", "into", "join",
			"let", "orderby", "partial", "remove", "select", "set", "value", "var", "where", "yield"
		};

		private static readonly string[] csTypes =
		{
			"bool", "byte", "sbyte", "short", "ushort", "int", "uint", "long", "ulong", "char", "decimal", "double", "float",
			"void", "string", "DateTime", "SqlGeometry", "SqlGeography", "SqlHierarchyId", "TimeSpan", "Guid", "object"
		};

		private static readonly string[] vbKeywords =
		{
			"AddHandler", "AddressOf", "Alias", "And", "AndAlso", "As", "ByRef", "ByVal", "Call", "Case", "Catch",
			"Class", "Const", "Continue", "CType", "Declare", "Default", "Delegate", "Dim", "DirectCast", "Do",
			"Each", "Else", "ElseIf", "End", "EndIf", "Enum", "Erase", "Error", "Event", "Exit", "False", "Finally",
			"For", "Friend", "Function", "Get", "GetType", "GetXMLNamespace", "Global", "GoSub", "GoTo", "Handles",
			"If", "Implements", "Imports", "In", "Inherits", "Integer", "Interface", "Is", "IsNot", "Let", "Lib",
			"Like", "Loop", "Me", "Mod", "Module", "MustInherit", "MustOverride", "MyBase", "MyClass", "Namespace",
			"Narrowing", "New", "Next", "Not", "Nothing", "NotInheritable", "NotOverridable", "Of", "On", "Operator",
			"Option", "Optional", "Or", "OrElse", "Out", "Overloads", "Overridable", "Overrides", "ParamArray",
			"Partial", "Private", "Property", "Protected", "Public", "RaiseEvent", "ReadOnly", "ReDim", "REM",
			"RemoveHandler", "Resume", "Return", "Select", "Set", "Shadows", "Shared", "Static", "Step", "Stop",
			"String", "Structure", "Sub", "SyncLock", "Then", "Throw", "To", "True", "Try", "TryCast", "TypeOf",
			"Using", "Wend", "When", "While", "Widening", "With", "WithEvents", "WriteOnly", "Xor", "#Const",
			"#Else", "#ElseIf", "#End", "#If", "Aggregate", "Ansi", "Assembly", "Async", "Auto", "Await", "Binary",
			"Compare", "Custom", "Distinct", "Equals", "Explicit", "From", "By", "Group", "Into", "IsFalse",
			"IsTrue", "Iterator", "Join", "Key", "Mid", "Off", "Order", "Preserve", "Skip", "Skip While", "Strict",
			"Take", "Text", "Unicode", "Until", "Where", "Yield ", "#ExternalSource", "#Region"
		};

		private static readonly string[] vbTypes =
		{
			"Boolean", "Byte", "CBool", "CByte", "CChar", "CDate", "CDbl", "CDec", "Char", "CInt", "CLng", "CObj",
			"CSByte", "CShort", "CSng", "CStr", "CUInt", "CULng", "CUShort", "Date", "Decimal", "Double", "Long",
			"Object", "SByte", "Short", "Single", "UInteger", "ULong", "UShort", "Variant", "SqlGeometry",
			"SqlGeography", "SqlHierarchyId", "TimeSpan", "Guid"
		};

		private static readonly string[] fsKeywords =
		{
			"asr", "land", "lor", "lsl", "lsr", "lxor", "mod", "sig", "atomic", "break", "checked", "component",
			"const", "constraint", "constructor", "continue", "eager", "event", "external", "fixed", "functor",
			"include", "method", "mixin", "object", "parallel", "process", "protected", "pure", "sealed", "tailcall",
			"trait", "virtual", "volatile", "abstract", "and", "as", "assert", "base", "begin", "class", "default",
			"delegate", "do", "done", "downcast", "downto", "elif", "else", "end", "exception", "extern", "FALSE",
			"finally", "for", "fun", "function", "global", "if", "in", "inherit", "inline", "interface", "internal",
			"lazy", "let", "match", "member", "module", "mutable", "namespace", "new", "not", "null", "of", "open",
			"or", "override", "private", "public", "rec", "return", "select", "static", "struct", "then", "to",
			"TRUE", "try", "type", "upcast", "use", "val", "void", "when", "while", "with", "yield"
		};

		private static readonly string[] fsTypes =
		{
			"bool", "byte", "sbyte", "int16", "uint16", "int", "uint32", "int64", "uint64", "nativeint",
			"unativeint", "char", "string", "decimal", "unit", "void", "float32, single", "float, double",
			"DateTime", "SqlGeometry", "SqlGeography", "SqlHierarchyId",
			"TimeSpan", "Guid"
		};

		public static void LoadLanguage(string language)
		{
			string[] keywords = null;
			string[] types = null;
			if (language == "C#")
			{
				keywords = csKeywords;
				types = csTypes;
			}
			else if (language == "F#")
			{
				keywords = fsKeywords;
				types = fsTypes;
				TagsList["Nullable"] = Color.Gray;
				foreach (string str in new []{ "array", "list", "option", "ref", "seq" })
				{
					if (!TagsList.ContainsKey(str))
					{
						TagsList[str] = FunctionColor;
					}
				}
			}
			else if (language == "VB.NET")
			{
				keywords = vbKeywords;
				types = vbTypes;
			}

			foreach (string str in keywords)
			{
				if (!TagsList.ContainsKey(str))
				{
					TagsList[str] = KeywordColor;
				}
			}

			foreach (string str in types)
			{
				if (!TagsList.ContainsKey(str))
				{
					TagsList[str] = TypeColor;
				}
			}
		}

		public static void UnloadLanguage()
		{
			TagsList.Clear();
		}

		public static void LoadWords(string words, Color color)
		{
			string[] strs = words.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);

			for (int index = 0; index < strs.Length; index++)
			{
				strs[index] = strs[index].Replace("\r", "");
			}

			foreach (string str in strs)
			{
				if (!TagsList.ContainsKey(str.ToLower()) && !TagsList.ContainsKey(str.ToUpper()))
				{
					TagsList[str.ToLower()] = color;
					TagsList[str.ToUpper()] = color;
				}
			}
		}

		public static void UnloadWords()
		{
			var indexesToDelete =
				TagsList.Where(
					pair =>
					pair.Value == FunctionNoAnsiColor).
				ToList();
			foreach (KeyValuePair<string, Color> pair in indexesToDelete)
			{
				TagsList.Remove(pair.Key);
			}
		}

		public static Dictionary<string, Color> Tags => TagsList;

		public static IEnumerable<Color> Colors => ColorList;

		public static bool IsKnownTag(string tag)
		{
			return TagsList.ContainsKey(tag);
		}
	}
}