
// This file has been generated by the GUI designer. Do not modify.
namespace SqlDynamiteX
{
	public partial class MainWindow
	{
		private global::Gtk.UIManager UIManager;

		private global::Gtk.Action btn_Search;

		private global::Gtk.Action btn_Stop;

		private global::Gtk.Action addAction;

		private global::Gtk.Action quitAction;

		private global::Gtk.Action preferencesAction;

		private global::Gtk.ToggleAction selectColorAction;

		private global::Gtk.ToggleAction convertAction;

		private global::Gtk.ToggleAction selectFontAction;

		private global::Gtk.Action infoAction;

		private global::Gtk.Action saveAction;

		private global::Gtk.Action propertiesAction;

		private global::Gtk.VBox vbox2;

		private global::Gtk.Toolbar toolbar2;

		private global::Gtk.HBox hbox1;

		private global::Gtk.Label label4;

		private global::Gtk.ComboBoxEntry Search;

		private global::Gtk.Label label5;

		private global::Gtk.ComboBox Connections;

		private global::Gtk.HBox hbox2;

		private global::Gtk.ScrolledWindow GtkScrolledWindow;

		private global::Gtk.TreeView list1;

		private global::Gtk.ScrolledWindow GtkScrolledWindow1;

		private global::Gtk.TextView box1;

		private global::Gtk.ProgressBar progressbar1;

		private global::Gtk.Statusbar statusbar1;

		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget SqlDynamiteX.MainWindow
			this.UIManager = new global::Gtk.UIManager();
			global::Gtk.ActionGroup w1 = new global::Gtk.ActionGroup("Default");
			this.btn_Search = new global::Gtk.Action("btn_Search", null, global::Mono.Unix.Catalog.GetString("Search"), "gtk-find");
			w1.Add(this.btn_Search, null);
			this.btn_Stop = new global::Gtk.Action("btn_Stop", null, global::Mono.Unix.Catalog.GetString("Stop"), "gtk-stop");
			this.btn_Stop.Sensitive = false;
			w1.Add(this.btn_Stop, null);
			this.addAction = new global::Gtk.Action("addAction", null, global::Mono.Unix.Catalog.GetString("Edit & add DB"), "gtk-add");
			w1.Add(this.addAction, null);
			this.quitAction = new global::Gtk.Action("quitAction", null, global::Mono.Unix.Catalog.GetString("Exit"), "gtk-quit");
			w1.Add(this.quitAction, null);
			this.preferencesAction = new global::Gtk.Action("preferencesAction", null, global::Mono.Unix.Catalog.GetString("Search preferences"), "gtk-preferences");
			w1.Add(this.preferencesAction, null);
			this.selectColorAction = new global::Gtk.ToggleAction("selectColorAction", null, global::Mono.Unix.Catalog.GetString("Syntax highlighting"), "gtk-select-color");
			w1.Add(this.selectColorAction, null);
			this.convertAction = new global::Gtk.ToggleAction("convertAction", null, global::Mono.Unix.Catalog.GetString("Word wrap"), "gtk-convert");
			w1.Add(this.convertAction, null);
			this.selectFontAction = new global::Gtk.ToggleAction("selectFontAction", null, global::Mono.Unix.Catalog.GetString("Case sensitive"), "gtk-select-font");
			w1.Add(this.selectFontAction, null);
			this.infoAction = new global::Gtk.Action("infoAction", null, global::Mono.Unix.Catalog.GetString("About"), "gtk-info");
			w1.Add(this.infoAction, null);
			this.saveAction = new global::Gtk.Action("saveAction", null, global::Mono.Unix.Catalog.GetString("Save"), "gtk-save");
			w1.Add(this.saveAction, null);
			this.propertiesAction = new global::Gtk.Action("propertiesAction", null, global::Mono.Unix.Catalog.GetString("Installed .NET Data Providers"), "gtk-properties");
			w1.Add(this.propertiesAction, null);
			this.UIManager.InsertActionGroup(w1, 0);
			this.AddAccelGroup(this.UIManager.AccelGroup);
			this.Name = "SqlDynamiteX.MainWindow";
			this.Title = global::Mono.Unix.Catalog.GetString("SQL Dynamite X");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			// Container child SqlDynamiteX.MainWindow.Gtk.Container+ContainerChild
			this.vbox2 = new global::Gtk.VBox();
			this.vbox2.Name = "vbox2";
			this.vbox2.Spacing = 6;
			// Container child vbox2.Gtk.Box+BoxChild
			this.UIManager.AddUiFromString(@"<ui><toolbar name='toolbar2'><toolitem name='btn_Search' action='btn_Search'/><toolitem name='btn_Stop' action='btn_Stop'/><separator/><toolitem name='saveAction' action='saveAction'/><toolitem name='addAction' action='addAction'/><toolitem name='preferencesAction' action='preferencesAction'/><toolitem name='propertiesAction' action='propertiesAction'/><toolitem name='infoAction' action='infoAction'/><toolitem name='quitAction' action='quitAction'/><separator/><toolitem name='selectColorAction' action='selectColorAction'/><toolitem name='convertAction' action='convertAction'/><toolitem name='selectFontAction' action='selectFontAction'/></toolbar></ui>");
			this.toolbar2 = ((global::Gtk.Toolbar)(this.UIManager.GetWidget("/toolbar2")));
			this.toolbar2.Name = "toolbar2";
			this.toolbar2.ShowArrow = false;
			this.toolbar2.ToolbarStyle = ((global::Gtk.ToolbarStyle)(0));
			this.toolbar2.IconSize = ((global::Gtk.IconSize)(2));
			this.vbox2.Add(this.toolbar2);
			global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.toolbar2]));
			w2.Position = 0;
			w2.Expand = false;
			w2.Fill = false;
			// Container child vbox2.Gtk.Box+BoxChild
			this.hbox1 = new global::Gtk.HBox();
			this.hbox1.Name = "hbox1";
			this.hbox1.Spacing = 6;
			// Container child hbox1.Gtk.Box+BoxChild
			this.label4 = new global::Gtk.Label();
			this.label4.Name = "label4";
			this.label4.LabelProp = global::Mono.Unix.Catalog.GetString("Find what: ");
			this.hbox1.Add(this.label4);
			global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.label4]));
			w3.Position = 0;
			w3.Expand = false;
			w3.Fill = false;
			// Container child hbox1.Gtk.Box+BoxChild
			this.Search = global::Gtk.ComboBoxEntry.NewText();
			this.Search.Name = "Search";
			this.hbox1.Add(this.Search);
			global::Gtk.Box.BoxChild w4 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.Search]));
			w4.Position = 1;
			w4.Expand = false;
			w4.Fill = false;
			// Container child hbox1.Gtk.Box+BoxChild
			this.label5 = new global::Gtk.Label();
			this.label5.Name = "label5";
			this.label5.LabelProp = global::Mono.Unix.Catalog.GetString("Search in DB: ");
			this.hbox1.Add(this.label5);
			global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.label5]));
			w5.Position = 2;
			w5.Expand = false;
			w5.Fill = false;
			// Container child hbox1.Gtk.Box+BoxChild
			this.Connections = global::Gtk.ComboBox.NewText();
			this.Connections.Name = "Connections";
			this.hbox1.Add(this.Connections);
			global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.Connections]));
			w6.Position = 3;
			w6.Expand = false;
			w6.Fill = false;
			this.vbox2.Add(this.hbox1);
			global::Gtk.Box.BoxChild w7 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.hbox1]));
			w7.Position = 1;
			w7.Expand = false;
			// Container child vbox2.Gtk.Box+BoxChild
			this.hbox2 = new global::Gtk.HBox();
			this.hbox2.Name = "hbox2";
			this.hbox2.Spacing = 6;
			// Container child hbox2.Gtk.Box+BoxChild
			this.GtkScrolledWindow = new global::Gtk.ScrolledWindow();
			this.GtkScrolledWindow.Name = "GtkScrolledWindow";
			this.GtkScrolledWindow.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child GtkScrolledWindow.Gtk.Container+ContainerChild
			this.list1 = new global::Gtk.TreeView();
			this.list1.CanFocus = true;
			this.list1.Name = "list1";
			this.GtkScrolledWindow.Add(this.list1);
			this.hbox2.Add(this.GtkScrolledWindow);
			global::Gtk.Box.BoxChild w9 = ((global::Gtk.Box.BoxChild)(this.hbox2[this.GtkScrolledWindow]));
			w9.Position = 0;
			// Container child hbox2.Gtk.Box+BoxChild
			this.GtkScrolledWindow1 = new global::Gtk.ScrolledWindow();
			this.GtkScrolledWindow1.Name = "GtkScrolledWindow1";
			this.GtkScrolledWindow1.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child GtkScrolledWindow1.Gtk.Container+ContainerChild
			this.box1 = new global::Gtk.TextView();
			this.box1.CanFocus = true;
			this.box1.Name = "box1";
			this.GtkScrolledWindow1.Add(this.box1);
			this.hbox2.Add(this.GtkScrolledWindow1);
			global::Gtk.Box.BoxChild w11 = ((global::Gtk.Box.BoxChild)(this.hbox2[this.GtkScrolledWindow1]));
			w11.Position = 1;
			this.vbox2.Add(this.hbox2);
			global::Gtk.Box.BoxChild w12 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.hbox2]));
			w12.Position = 2;
			// Container child vbox2.Gtk.Box+BoxChild
			this.progressbar1 = new global::Gtk.ProgressBar();
			this.progressbar1.Name = "progressbar1";
			this.vbox2.Add(this.progressbar1);
			global::Gtk.Box.BoxChild w13 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.progressbar1]));
			w13.Position = 3;
			w13.Expand = false;
			w13.Fill = false;
			// Container child vbox2.Gtk.Box+BoxChild
			this.statusbar1 = new global::Gtk.Statusbar();
			this.statusbar1.Name = "statusbar1";
			this.statusbar1.Spacing = 6;
			this.vbox2.Add(this.statusbar1);
			global::Gtk.Box.BoxChild w14 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.statusbar1]));
			w14.Position = 4;
			w14.Expand = false;
			w14.Fill = false;
			this.Add(this.vbox2);
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.DefaultWidth = 689;
			this.DefaultHeight = 519;
			this.Show();
			this.DeleteEvent += new global::Gtk.DeleteEventHandler(this.OnDeleteEvent);
			this.btn_Search.Activated += new global::System.EventHandler(this.OnMediaPlayActionToggled);
			this.btn_Stop.Activated += new global::System.EventHandler(this.OnStopActionToggled);
			this.addAction.Activated += new global::System.EventHandler(this.OnAddActionToggled);
			this.quitAction.Activated += new global::System.EventHandler(this.OnQuitActionToggled);
			this.preferencesAction.Activated += new global::System.EventHandler(this.OnPreferencesActionActivated);
			this.selectColorAction.Toggled += new global::System.EventHandler(this.OnSelectColorActionToggled);
			this.convertAction.Toggled += new global::System.EventHandler(this.OnConvertActionToggled);
			this.selectFontAction.Toggled += new global::System.EventHandler(this.OnSelectFontActionToggled);
			this.infoAction.Activated += new global::System.EventHandler(this.OnInfoActionActivated);
			this.saveAction.Activated += new global::System.EventHandler(this.OnSaveActionActivated);
			this.propertiesAction.Activated += new global::System.EventHandler(this.Drivers_Activated);
		}
	}
}
