using System.Reflection;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyTitle("SqlDynamiteX")]
[assembly: AssemblyDescription("Easy text search in stored procedures, functions, tables, views and triggers.\nSupports MSSQL, Oracle, SAP, Firebird, Mysql, PostgreSQL, DB2, Informix, SQLite, Ingres, SQL Azure, Azure Redis Cache, Interbase, Vertica, MongoDb, Redis, Cassandra, Neo4j, InfluxDb.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fire Lizard Software")]
[assembly: AssemblyProduct("SQL Dynamite")]
[assembly: AssemblyCopyright("Copyright � Anatoliy Sova 2005 - 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[assembly: AssemblyVersion("2.5.1.4")]

// The following attributes are used to specify the signing key for the assembly, 
// if desired. See the Mono documentation for more information about signing.

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]
