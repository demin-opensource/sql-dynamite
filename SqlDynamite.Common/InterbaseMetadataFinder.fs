﻿namespace SqlDynamite.Common

open System

type InterbaseMetadataFinder() =
    inherit FirebirdMetadataFinder()

    override this.GetSequenceDefinition(sequenceName:string) : string =
        let query = String.Format("select gen_id( {0} , 0) from rdb$database;", sequenceName)
        let value = this.CreateDataSet(query).Tables.Item(0).Rows.Item(0).Item(0).ToString()
        let part1 = "CREATE GENERATOR " + sequenceName.Trim() + ";" + Environment.NewLine
        let part2 = "SET GENERATOR " + sequenceName.Trim() + " TO " + value + ";"
        part1 + part2
