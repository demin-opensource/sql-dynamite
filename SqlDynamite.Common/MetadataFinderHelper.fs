﻿namespace SqlDynamite.Common

open System
open System.Data
open System.Data.Common
open System.IO
open System.Reflection
open FirebirdSql.Data.FirebirdClient
open System.Data.SQLite
open MySql.Data.MySqlClient
open Npgsql
open Oracle.ManagedDataAccess.Client
open MongoDB.Driver
open MongoDB.Bson
open Cassandra.Data
open ServiceStack.Redis
open Neo4j.Driver.V1
open Vibrant.InfluxDB.Client
open Microsoft.AnalysisServices.AdomdClient

type MetadataFinderHelper() =

    static let DirSeparator = sprintf "%c" Path.DirectorySeparatorChar
    static let currentDirectory = Directory.GetCurrentDirectory() + DirSeparator
    static let interbase_dll = currentDirectory + "Borland.Data.AdoDbxClient.dll"
    static let ingres_dll = currentDirectory + "ingres.client.dll"
    static let advantage_dll = currentDirectory + "Advantage.Data.Provider.dll"
    static let anywhere_dll = currentDirectory + "Sap.Data.SQLAnywhere.v3.5.dll"
    static let ase_dll = currentDirectory + "Sybase.AdoNet4.AseClient.dll"
    static let ultralite_dll = currentDirectory + "Sap.Data.UltraLite.dll"
    static let hana_dll = currentDirectory + "Sap.Data.Hana.v3.5.dll"
    static let vertica_dll = currentDirectory + "Vertica.Data.dll"
    
    static let _providers = [("Interbase","Borland.Data.AdoDbxClient");("Ingres","Ingres.Client");
                             ("Informix","IBM.Data.Informix");("DB2","IBM.Data.DB2");
                             ("SAP ASE","Sybase.AdoNet4.AseClient");("SAP Anywhere","Sap.Data.SQLAnywhere");
                             ("UltraLite","Sap.Data.UltraLite");("SAP Advantage","Advantage.Data.Provider");
                             ("SAP HANA","Sap.Data.Hana");("Vertica","Vertica.Data")]

    static member SystemDbs = dict [("MSSQL",["master";"model";"tempdb";"msdb"]);
                               ("SQL Analysis Services",[]);
                               ("SQL Azure",["master"]);
                               ("SAP ASE",["master";"model";"tempdb";"sybsystemdb";"sybsystemprocs"]);
                               ("MySQL",["mysql";"sys";"information_schema";"performance_schema"]);
                               ("PostgreSQL",["postgres";"template0";"template1"]);
                               ("Informix",["sysadmin";"sysmaster";"sysuser";"sysutils"]);
                               ("Redis",[]);
                               ("MongoDb",["admin";"config";"local"]);
                               ("InfluxDb",["_internal"]);
                               ("Cassandra",["system";"system_auth";"system_distributed";"system_schema";"system_traces"])]

    static member ProvidersList : (string * string) list =
        let platform = Environment.OSVersion.Platform
        let gacFolders = ["gac";"GAC_MSIL";"GAC_32";"GAC_64"]
        let paths =
            if platform = PlatformID.Win32NT then [@"%systemroot%\assembly"; @"%systemroot%\Microsoft.NET\assembly"]
            elif platform = PlatformID.Unix && Environment.OSVersion.Version.Major >= 8 || platform = PlatformID.MacOSX then ["/Library/Frameworks/Mono.framework/Libraries/mono"]
            elif platform = PlatformID.Unix && Environment.OSVersion.Version.Major <= 7 then ["/usr/lib/mono"]
            else []
        let rec loop gacFolder n acc =
            if n > 0 then
                let pathStr = Path.Combine(Environment.ExpandEnvironmentVariables(paths.Item(n - 1)), gacFolder)
                if Directory.Exists(pathStr) then
                    loop gacFolder (n - 1) acc @ MetadataFinderHelper.CheckAssemblies(pathStr)
                else
                    loop gacFolder (n - 1) acc
            else
                acc
        let rec extLoop n acc =
            if n > 0 then
                extLoop (n - 1) acc @ loop (gacFolders.Item(n - 1)) paths.Length []
            else
                acc
        extLoop gacFolders.Length []
    
    static member CheckAssemblies(path:string) : (string * string) list =
        let assemblyFolders = Directory.GetDirectories(path)
        let rec loop (str:string) n acc =
            if n > 0 then
                if str.ToLower().StartsWith((snd (_providers.Item(n - 1))).ToLower()) then
                    let directories = Directory.GetDirectories(path + DirSeparator + str)
                    if directories.Length > 0 then
                        let info = directories.[0].Split([|"__"|], StringSplitOptions.None)
                        if info.Length > 1  && not (info.[0].Contains(".EF6") || info.[0].Contains(".Entity") || info.[0].Contains(".ASP")) then
                            let parts = info.[0].Replace(path + DirSeparator + str + DirSeparator, "").Split('_')
                            let version = if parts.Length = 1 then parts.[0] else parts.[1]
                            let publicKeyToken = info.[1]
                            loop str (n - 1) ((fst (_providers.Item(n - 1)), str + ", Version=" + version + ", Culture=neutral, PublicKeyToken=" + publicKeyToken) :: acc)
                        else
                            loop str (n - 1) acc
                    else
                        loop str (n - 1) acc
                else
                    loop str (n - 1) acc
            else
                acc
        let rec extLoop n acc =
            if n > 0 then
                let str = assemblyFolders.[n - 1].Replace(path + DirSeparator, "")
                extLoop (n - 1) acc @ loop str _providers.Length []
            else
                acc
        extLoop assemblyFolders.Length []
    
    static member CreateTypeInstance(serverType:string, assemblyFileName:string, typeName:string, [<ParamArray>] args : obj array) : obj =
        let gacAssembly =
            if List.exists (fun item -> fst item = serverType) MetadataFinderHelper.ProvidersList
                then MetadataFinderHelper.ProvidersList |> List.find (fun item -> fst item = serverType) |> snd
                else null
        let assembly = 
            if gacAssembly = null then 
                if assemblyFileName <> null then
                    Assembly.LoadFile(assemblyFileName) 
                else
                    raise (new ArgumentException(String.Format("Could not find data provider for {0}", serverType)))
            else 
                Assembly.Load(gacAssembly)
        let typ = assembly.GetType(typeName)
        Activator.CreateInstance(typ, args)
    
    static member CreateConnection(finder, connectionString:string) : DbConnection =
        match finder.GetType().Name with
        | "FirebirdMetadataFinder" -> new FbConnection(connectionString) :> DbConnection
        | "SqliteMetadataFinder" -> new SQLiteConnection(connectionString) :> DbConnection
        | "MySqlMetadataFinder" -> new MySqlConnection(connectionString) :> DbConnection
        | "PostgreSqlMetadataFinder" -> new NpgsqlConnection(connectionString) :> DbConnection
        | "OracleMetadataFinder" -> new OracleConnection(connectionString) :> DbConnection
        | "MongoDbMetadataFinder" -> new MongoConnection(ConnectionString = connectionString) :> DbConnection
        | "RedisMetadataFinder" -> new RedisConnection(ConnectionString = connectionString) :> DbConnection
        | "Neo4jMetadataFinder" -> new Neo4jConnection(ConnectionString = connectionString) :> DbConnection
        | "InfluxDbMetadataFinder" -> new InfluxConnection(ConnectionString = connectionString) :> DbConnection
        | "CassandraMetadataFinder" -> new CqlConnection(connectionString) :> DbConnection
        | "InterbaseMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("Interbase", interbase_dll, "Borland.Data.TAdoDbxConnection", connectionString) :?> DbConnection
        | "IngresMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("Ingres", ingres_dll, "Ingres.Client.IngresConnection", connectionString) :?> DbConnection
        | "InformixMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("Informix", null, "IBM.Data.Informix.IfxConnection", connectionString) :?> DbConnection
        | "Db2MetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("DB2", null, "IBM.Data.DB2.DB2Connection", connectionString) :?> DbConnection
        | "VerticaMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("Vertica", vertica_dll, "Vertica.Data.VerticaClient.VerticaConnection", connectionString) :?> DbConnection
        | "SybaseAnywhereMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("SAP Anywhere", anywhere_dll, "Sap.Data.SQLAnywhere.SAConnection", connectionString) :?> DbConnection
        | "SybaseMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("SAP ASE", ase_dll, "Sybase.Data.AseClient.AseConnection", connectionString) :?> DbConnection
        | "SapHanaMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("SAP HANA", hana_dll, "Sap.Data.Hana.HanaConnection", connectionString) :?> DbConnection
        | "UltraliteMetadataFinder" -> MetadataFinderHelper.CreateTypeInstance("UltraLite", ultralite_dll, "Sap.Data.UltraLite.ULConnection", connectionString) :?> DbConnection
        | "SybaseAdvantageMetadataFinder" -> 
                                    let innerConnection = MetadataFinderHelper.CreateTypeInstance("SAP Advantage", advantage_dll, "Advantage.Data.Provider.AdsConnection", connectionString)
                                    new WrapperConnection(innerConnection :?> IDbConnection) :> DbConnection
        | "SsasMetadataFinder" -> 
                                    new WrapperConnection(new AdomdConnection(connectionString)) :> DbConnection
        | _ -> null

    static member CreateDataSet(connection:DbConnection, query:string) : DataSet =
        match connection with
        | :? FbConnection ->
            let command = new FbCommand(query, connection :?> FbConnection)
            command.CommandTimeout <- 3600
            let adapter = new FbDataAdapter(command)
            let dataSet = new DataSet()
            ignore(adapter.Fill(dataSet))
            dataSet
        | :? SQLiteConnection ->
            let command = new SQLiteCommand(query, connection :?> SQLiteConnection)
            command.CommandTimeout <- 3600
            let adapter = new SQLiteDataAdapter(command)
            let dataSet = new DataSet()
            ignore(adapter.Fill(dataSet))
            dataSet
        | :? MySqlConnection ->
            let command = new MySqlCommand(query, connection :?> MySqlConnection)
            command.CommandTimeout <- 3600
            let adapter = new MySqlDataAdapter(command)
            let dataSet = new DataSet()
            ignore(adapter.Fill(dataSet))
            dataSet
        | :? NpgsqlConnection ->
            let command = new NpgsqlCommand(query, connection :?> NpgsqlConnection)
            command.CommandTimeout <- 3600
            let adapter = new NpgsqlDataAdapter(command)
            let dataSet = new DataSet()
            ignore(adapter.Fill(dataSet))
            dataSet
        | :? OracleConnection ->
            let command = new OracleCommand(query, connection :?> OracleConnection)
            command.CommandTimeout <- 3600
            let adapter = new OracleDataAdapter(command)
            let dataSet = new DataSet()
            let _ = adapter.Fill(dataSet)
            dataSet
        | :? MongoConnection ->
            let dataSet = new DataSet()
            let dataTable = new DataTable()
            ignore(dataTable.Columns.Add("Name"))
            ignore(dataTable.Columns.Add("ObjectType"))
            
            let mongoUrl = new MongoUrl(connection.ConnectionString)
            let mongoUrlDb = mongoUrl.DatabaseName
            let connStr = if connection.ConnectionString.Contains("mongodb.net") && mongoUrlDb <> null then connection.ConnectionString.Replace("/" + mongoUrlDb.ToString() + "?ssl=true", "?ssl=true") else connection.ConnectionString
            let client = new MongoClient(connStr)
            let databaseNames = client.ListDatabaseNames()
            let databasesExist = databaseNames.MoveNext()
            if not databasesExist then raise (Exception("Databases do not exist in the selected cluster"))
            let databaseNameList = databaseNames.Current :?> System.Collections.Generic.List<string>
            let dbname = if mongoUrlDb = null then databaseNameList.[0] else mongoUrlDb.ToString()
            let database = client.GetDatabase(dbname)
            
            let collections = database.ListCollectionNames()
            let _ = collections.MoveNext()
            let collectionNameList = collections.Current
            for col in collectionNameList do
                let colName = col.ToString()
                if colName.Contains(query) then
                    let dataRow = dataTable.NewRow()
                    dataRow.Item("Name") <- dbname + " : " + colName
                    dataRow.Item("ObjectType") <- "TABLE"
                    dataTable.Rows.Add(dataRow)

                try
                    let collection = database.GetCollection(colName)
                    let indexes = collection.Indexes
                    let indexNames = indexes.List()
                    let _ = indexNames.MoveNext()
                    let indexNameList = indexNames.Current
                    for index in indexNameList do
                        let name = index.GetValue("name").ToString()
                        if name <> "_id_" && name.Contains(query) then
                            let dataRow = dataTable.NewRow()
                            dataRow.Item("Name") <- dbname + " : " + colName + " : " + name
                            dataRow.Item("ObjectType") <- "INDEX"
                            dataTable.Rows.Add(dataRow)
                with
                    exc -> 
                        if exc.InnerException <> null && exc.InnerException :? MongoCommandException then
                            dataTable.Rows.Item(dataTable.Rows.Count - 1).Item("ObjectType") <- "VIEW"
                        else
                            ()
            
            try
                let systemcollection = database.GetCollection("system.js")
                let bsonDocument = new BsonDocument()
                let bsonDocumentFilterDefinition = new BsonDocumentFilterDefinition<BsonDocument>(bsonDocument)
                let functions = systemcollection.Find(bsonDocumentFilterDefinition)
                let cursor = functions.ToCursor()
                let _ = cursor.MoveNext()
                let funcList = cursor.Current
                for fn in funcList do
                    let fnName = fn.GetValue("_id").ToString()
                    if fnName.Contains(query) then
                        let dataRow = dataTable.NewRow()
                        dataRow.Item("Name") <- dbname + " : " + fnName
                        dataRow.Item("ObjectType") <- "FUNCTION"
                        dataTable.Rows.Add(dataRow)
            with
                _ -> ()
            dataSet.Tables.Add(dataTable)
            dataSet
        | :? CqlConnection ->
            let getData (ds:DataSet, script) =
                let command = new CqlCommand()
                command.CommandText <- script
                command.Connection <- connection
                let adapter = new CqlDataAdapter()
                adapter.SelectCommand <- command
                ignore(adapter.Fill(ds))
            let dataSet = new DataSet()
            if query.Split(' ').Length > 1 then
                getData(dataSet, query)
            else
                let keyspace = if connection.ConnectionString.ToLower().Contains("default keyspace=") then connection.ConnectionString.ToLower().Replace("default keyspace=", "").Split(';').[0] else null
                getData(dataSet, "select AGGREGATE_NAME, KEYSPACE_NAME from SYSTEM_SCHEMA.AGGREGATES")
                let aggregates = [for row in dataSet.Tables.[0].Rows do yield (row.Item("AGGREGATE_NAME").ToString(), row.Item("KEYSPACE_NAME").ToString())]
                dataSet.Clear()
                getData(dataSet, "select TRIGGER_NAME, KEYSPACE_NAME from SYSTEM_SCHEMA.TRIGGERS")
                let triggers = [for row in dataSet.Tables.[0].Rows do yield (row.Item("TRIGGER_NAME").ToString(), row.Item("KEYSPACE_NAME").ToString())]
                dataSet.Clear()
                getData(dataSet, "select VIEW_NAME, KEYSPACE_NAME from SYSTEM_SCHEMA.VIEWS")
                let views = [for row in dataSet.Tables.[0].Rows do yield (row.Item("VIEW_NAME").ToString(), row.Item("KEYSPACE_NAME").ToString())]
                dataSet.Clear()
                getData(dataSet, "select TABLE_NAME, KEYSPACE_NAME from SYSTEM_SCHEMA.TABLES")
                let tables = [for row in dataSet.Tables.[0].Rows do yield (row.Item("TABLE_NAME").ToString(), row.Item("KEYSPACE_NAME").ToString())]
                dataSet.Clear()
                getData(dataSet, "select INDEX_NAME, KEYSPACE_NAME from SYSTEM_SCHEMA.INDEXES")
                let indexes = [for row in dataSet.Tables.[0].Rows do yield (row.Item("INDEX_NAME").ToString(), row.Item("KEYSPACE_NAME").ToString())]
                dataSet.Clear()
                getData(dataSet, "select TYPE_NAME, KEYSPACE_NAME from SYSTEM_SCHEMA.TYPES")
                let types = [for row in dataSet.Tables.[0].Rows do yield (row.Item("TYPE_NAME").ToString(), row.Item("KEYSPACE_NAME").ToString())]
                dataSet.Clear()
                getData(dataSet, "select TABLE_NAME, COLUMN_NAME, KEYSPACE_NAME from SYSTEM_SCHEMA.COLUMNS")
                let columns = [for row in dataSet.Tables.[0].Rows do yield [row.Item("TABLE_NAME").ToString(); row.Item("COLUMN_NAME").ToString(); row.Item("KEYSPACE_NAME").ToString()]]
                dataSet.Clear()
                getData(dataSet, "select FUNCTION_NAME, BODY, KEYSPACE_NAME from SYSTEM_SCHEMA.FUNCTIONS")
                let functions = [for row in dataSet.Tables.[0].Rows do yield [row.Item("FUNCTION_NAME").ToString(); row.Item("BODY").ToString(); row.Item("KEYSPACE_NAME").ToString()]]
                dataSet.Clear()
                let list = new System.Collections.Generic.List<(string * string)>()
                for item in aggregates do if (fst item).Contains(query) && (String.IsNullOrWhiteSpace(keyspace) || snd item = keyspace) then list.Add((fst item, "AGGREGATE"))
                for item in triggers do if (fst item).Contains(query) && (String.IsNullOrWhiteSpace(keyspace) || snd item = keyspace) then list.Add((fst item, "TRIGGER"))
                for item in views do if (fst item).Contains(query) && (String.IsNullOrWhiteSpace(keyspace) || snd item = keyspace) then list.Add((fst item, "VIEW"))
                for item in tables do if (fst item).Contains(query) && (String.IsNullOrWhiteSpace(keyspace) || snd item = keyspace) then list.Add((fst item, "TABLE"))
                for item in indexes do if (fst item).Contains(query) && (String.IsNullOrWhiteSpace(keyspace) || snd item = keyspace) then list.Add((fst item, "INDEX"))
                for item in types do if (fst item).Contains(query) && (String.IsNullOrWhiteSpace(keyspace) || snd item = keyspace) then list.Add((fst item, "TYPE"))
                for item in functions do if (item.Item(0).Contains(query) || item.Item(1).Contains(query)) && (String.IsNullOrWhiteSpace(keyspace) || item.Item(2) = keyspace) then list.Add((item.Item(0), "FUNCTION"))
                for item in columns do
                    if item.Item(1).Contains(query) && (String.IsNullOrWhiteSpace(keyspace) || item.Item(2) = keyspace) then
                        let isTable = List.exists (fun elem -> fst elem = item.Item(0)) tables
                        if isTable then
                            list.Add((item.Item(0), "TABLE"))
                        else
                            list.Add((item.Item(0), "VIEW"))
                    else
                        ()
                let dataTable = new DataTable()
                ignore(dataTable.Columns.Add("Name"))
                ignore(dataTable.Columns.Add("ObjectType"))
                for item in list do
                    let dataRow = dataTable.NewRow()
                    dataRow.Item("Name") <- fst item
                    dataRow.Item("ObjectType") <- snd item
                    dataTable.Rows.Add(dataRow)
                dataSet.Tables.RemoveAt(0)
                dataSet.Tables.Add(dataTable)
            dataSet
        | :? RedisConnection ->
            let dataSet = new DataSet()
            let dataTable = new DataTable()
            ignore(dataTable.Columns.Add("Name"))
            ignore(dataTable.Columns.Add("ObjectType"))
            let parts = connection.ConnectionString.Split(';')
            let cs = parts.[0].Split(':')
            let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
            let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
            let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
            let client =
                if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
                else new RedisClient(cs.[0], port, password, int64(0))
            client.Db <- int64(db)
            let keys = client.GetAllKeys()
            for key in keys do
                if key.Contains(query) then
                    let dataRow = dataTable.NewRow()
                    dataRow.Item("Name") <- key
                    dataRow.Item("ObjectType") <- client.Type(key).ToUpper()
                    dataTable.Rows.Add(dataRow)
            dataSet.Tables.Add(dataTable)
            dataSet
        | :? Neo4jConnection ->
            let dataSet = new DataSet()
            let dataTable = new DataTable()
            ignore(dataTable.Columns.Add("Name"))
            ignore(dataTable.Columns.Add("ObjectType"))

            let parts = connection.ConnectionString.Split(';')
            let url = parts.[0]
            let username = parts.[1]
            let password = parts.[2]
            let authToken = if (String.IsNullOrWhiteSpace(password) || String.IsNullOrWhiteSpace(username)) then AuthTokens.None else AuthTokens.Basic(username, password)

            let driver = GraphDatabase.Driver(url, authToken)
            let indexes = driver.Session().Run(String.Format("call db.indexes()
            YIELD indexName AS name
            WITH name
            WHERE name CONTAINS '{0}'
            RETURN name", query))
            for idx in indexes do
                ignore(dataTable.Rows.Add(idx.Item("name"), "INDEX"))
            let labels = driver.Session().Run(String.Format("call db.labels()
            YIELD label
            WITH label
            WHERE label CONTAINS '{0}'
            RETURN label", query))
            for label in labels do
                ignore(dataTable.Rows.Add(label.Item("label"), "LABEL"))

            dataSet.Tables.Add(dataTable)
            dataSet
        | :? InfluxConnection ->
            let dataSet = new DataSet()
            let dataTable = new DataTable()
            ignore(dataTable.Columns.Add("Name"))
            ignore(dataTable.Columns.Add("ObjectType"))
            let parts = connection.ConnectionString.Split(';')
            let url = parts.[0]
            let databaseName = if parts.Length = 2 then parts.[1] elif parts.Length = 4 then parts.[3] else null
            let username = if parts.Length > 2 then parts.[1] else null
            let password = if parts.Length > 2 then parts.[2] else null
        
            let client = if parts.Length > 2 then new InfluxClient(new Uri(url), username, password) else new InfluxClient(new Uri(url))

            let measurements = async {
                return! client.ShowMeasurementsAsync(databaseName) |> Async.AwaitTask
            }
            let mrs = measurements |> Async.RunSynchronously
            if mrs.Series.Count > 0 then
                for row in mrs.Series.Item(0).Rows do
                    if row.Name.Contains(query) then
                        ignore(dataTable.Rows.Add(row.Name, "MEASUREMENT"))

            dataSet.Tables.Add(dataTable)
            dataSet
        | _ ->
            match connection.GetType().FullName with
            | "Borland.Data.TAdoDbxConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("Interbase", interbase_dll, "Borland.Data.TAdoDbxCommand") :?> DbCommand
                command.CommandText <- query
                command.Connection <- connection
                command.CommandTimeout <- 3600
                let adapter = MetadataFinderHelper.CreateTypeInstance("Interbase", interbase_dll, "Borland.Data.TAdoDbxDataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "Ingres.Client.IngresConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("Ingres", ingres_dll, "Ingres.Client.IngresCommand", query, connection) :?> DbCommand
                command.CommandTimeout <- 3600
                let adapter = MetadataFinderHelper.CreateTypeInstance("Ingres", ingres_dll, "Ingres.Client.IngresDataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "IBM.Data.Informix.IfxConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("Informix", null, "IBM.Data.Informix.IfxCommand", query, connection) :?> DbCommand
                command.CommandTimeout <- 3600
                let adapter = MetadataFinderHelper.CreateTypeInstance("Informix", null, "IBM.Data.Informix.IfxDataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "IBM.Data.DB2.DB2Connection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("DB2", null, "IBM.Data.DB2.DB2Command", query, connection) :?> DbCommand
                command.CommandTimeout <- 3600
                let adapter = MetadataFinderHelper.CreateTypeInstance("DB2", null, "IBM.Data.DB2.DB2DataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "SqlDynamite.Common.WrapperConnection" ->
                let wrapperConnection = connection :?> WrapperConnection
                match wrapperConnection.InnerConnection.GetType().FullName with
                | "Advantage.Data.Provider.AdsConnection" ->
                    let adsCommand = MetadataFinderHelper.CreateTypeInstance("SAP Advantage", advantage_dll, "Advantage.Data.Provider.AdsCommand", query, wrapperConnection.InnerConnection) :?> IDbCommand
                    let command = new WrapperCommand(adsCommand)
                    command.CommandTimeout <- 3600
                    let adapter = MetadataFinderHelper.CreateTypeInstance("SAP Advantage", advantage_dll, "Advantage.Data.Provider.AdsDataAdapter", command.InnerCommand) :?> DbDataAdapter
                    let dataSet = new DataSet()
                    ignore(adapter.Fill(dataSet))
                    dataSet
                | "Microsoft.AnalysisServices.AdomdClient.AdomdConnection" ->
                    let adomdCommand = new AdomdCommand(query, wrapperConnection.InnerConnection :?> AdomdConnection)
                    let command = new WrapperCommand(adomdCommand)
                    command.CommandTimeout <- 3600
                    let adapter = new AdomdDataAdapter(command.InnerCommand :?> AdomdCommand)
                    let dataSet = new DataSet()
                    ignore(adapter.Fill(dataSet))
                    dataSet
                | _ -> null
            | "Sap.Data.SQLAnywhere.SAConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("SAP Anywhere", anywhere_dll, "Sap.Data.SQLAnywhere.SACommand", query, connection) :?> DbCommand
                command.CommandTimeout <- 3600
                let adapter = MetadataFinderHelper.CreateTypeInstance("SAP Anywhere", anywhere_dll, "Sap.Data.SQLAnywhere.SADataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "Sybase.Data.AseClient.AseConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("SAP ASE", ase_dll, "Sybase.Data.AseClient.AseCommand", query, connection) :?> DbCommand
                command.CommandTimeout <- 3600
                let adapter = MetadataFinderHelper.CreateTypeInstance("SAP ASE", ase_dll, "Sybase.Data.AseClient.AseDataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "Sap.Data.UltraLite.ULConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("UltraLite", ultralite_dll, "Sap.Data.UltraLite.ULCommand", query, connection) :?> DbCommand
                let adapter = MetadataFinderHelper.CreateTypeInstance("UltraLite", ultralite_dll, "Sap.Data.UltraLite.ULDataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "Sap.Data.Hana.HanaConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("SAP HANA", hana_dll, "Sap.Data.Hana.HanaCommand", query, connection) :?> DbCommand
                let adapter = MetadataFinderHelper.CreateTypeInstance("SAP HANA", hana_dll, "Sap.Data.Hana.HanaDataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | "Vertica.Data.VerticaClient.VerticaConnection" ->
                let command = MetadataFinderHelper.CreateTypeInstance("Vertica", vertica_dll, "Vertica.Data.VerticaClient.VerticaCommand", query, connection) :?> DbCommand
                let adapter = MetadataFinderHelper.CreateTypeInstance("Vertica", vertica_dll, "Vertica.Data.VerticaClient.VerticaDataAdapter", command) :?> DbDataAdapter
                let dataSet = new DataSet()
                ignore(adapter.Fill(dataSet))
                dataSet
            | _ -> null

    static member UseDatabase(connection:DbConnection, database:string) =
        match connection with
        | :? MySqlConnection ->
                let cmd = new MySqlCommand("use `" + database + "`", connection :?> MySqlConnection)
                ignore(cmd.ExecuteNonQuery())
        | _ ->
            match connection.GetType().FullName with
            | "Sybase.Data.AseClient.AseConnection" ->
                    let cmd = MetadataFinderHelper.CreateTypeInstance("SAP ASE", ase_dll, "Sybase.Data.AseClient.AseCommand", "use " + database, connection) :?> DbCommand
                    ignore(cmd.ExecuteNonQuery())
            | _ -> ()
