﻿namespace SqlDynamite.Common

open System
open System.Data
open Vibrant.InfluxDB.Client

type InfluxDbMetadataFinder() =
    inherit MetadataFinder()

    override this.GetDatabaseList (scname:string) : string list =
        let parts = this._connection.ConnectionString.Split(';')
        let url = parts.[0]
        let username = if parts.Length > 2 then parts.[1] else null
        let password = if parts.Length > 2 then parts.[2] else null
        
        let client = if parts.Length > 2 then new InfluxClient(new Uri(url), username, password) else new InfluxClient(new Uri(url))
        let databases = async {
            return! client.ShowDatabasesAsync() |> Async.AwaitTask
        }
        let dbs = databases |> Async.RunSynchronously
        if dbs.Series.Count > 0 then
            [for row in dbs.Series.Item(0).Rows do yield row.Name]
        else
            []

    override this.Compose(tablename:string, fields:DataTable, keys:DataTable, checks:DataTable) : string =
        raise (NotImplementedException())

    override this.GetDefColumns(name:string, objtype:ObjectType) : DataTable list =
        [null; null; null]

    override this.GetTextColumns(name:string, objtype:ObjectType) : string list =
        let parts = this._connection.ConnectionString.Split(';')
        let url = parts.[0]
        let databaseName = if parts.Length = 2 then parts.[1] elif parts.Length = 4 then parts.[3] else null
        let username = if parts.Length > 2 then parts.[1] else null
        let password = if parts.Length > 2 then parts.[2] else null
        
        let client = if parts.Length > 2 then new InfluxClient(new Uri(url), username, password) else new InfluxClient(new Uri(url))

        let series = async {
            return! client.ShowSeriesAsync(databaseName, name) |> Async.AwaitTask
        }
        let srs = series |> Async.RunSynchronously
        if srs.Series.Count > 0 then
            [for row in srs.Series.Item(0).Rows do yield row.Key + Environment.NewLine]
        else
            []

    override this.GetNameByTextColumns(searchStr:string, types:ObjectType list, caseSensitive:bool) : string =
        searchStr

    override this.GenerateNameScript(search:string, types:ObjectType list, caseSensitive:bool) : string =
        search

    override this.GenerateSearchScript(search:string, caseSensitive:bool) : string =
        search

    override this.GetForeignKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetPrimaryKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetIndexDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetSequenceDefinition(sequenceName:string) : string =
        raise (NotImplementedException())

    override this.GetSynonymDefinition(synonymName:string) : string =
        raise (NotImplementedException())

    override this.GetTypeDefinition(typeName:string) : string =
        raise (NotImplementedException())

    override this.GetJobDefinition(jobName:string) : string =
        raise (NotImplementedException())

    override this.GetReportDefinition(reportName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedProcedureDefinition(procName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedTriggerDefinition(triggerName:string) : string =
        raise (NotImplementedException())
