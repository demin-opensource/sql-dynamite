﻿namespace SqlDynamite.Common

open MongoDB.Driver

type MongoConnection() =
    inherit NoSqlConnection()

    override this.Open () =
        let mongoUrl = new MongoUrl(this._connectionString)
        let dbname = mongoUrl.DatabaseName
        let connStr = if this._connectionString.Contains("mongodb.net") && dbname <> null then this._connectionString.Replace("/" + dbname.ToString() + "?ssl=true", "?ssl=true") else this._connectionString
        let client = new MongoClient(connStr)
        ignore(client.ListDatabaseNames())
        ()
