﻿namespace SqlDynamite.Common

open System
open System.Data

type PostgreSqlMetadataFinder() =
    inherit MetadataFinder()

    override this.Compose(tablename:string, fields:DataTable, keys:DataTable, checks:DataTable) : string =
        let result = "CREATE TABLE " + tablename + " ("
        let rowdef index =
            let record = fields.Rows.Item(index)
            let part1 = "\n\t" + record.Item("column_name").ToString() + " " + record.Item("type_name").ToString() + " "
            let rtn = record.Item("type_name").ToString()
            let part2 =
                match rtn.ToLower() with
                | "char" | "character varying" ->
                    "(" + record.Item("length").ToString().Trim() + " ) "
                | "decimal" ->
                    "(" + record.Item("precision").ToString().Trim() + ", " + record.Item("scale").ToString().Trim() + " ) "
                | _ -> ""
            let nullable = record.Item("nullable").ToString().Trim()
            let nulldef = if nullable = "1" then "NULL" else "NOT NULL"
            part1 + part2 + nulldef + ","
        let rec loop n acc =
            if n > 0 then
                loop (n - 1) (rowdef(n - 1) :: acc)
            else
                acc
        result + String.Join("", loop fields.Rows.Count []) + Environment.NewLine + ")"

    member this.ComposeTableType(typename:string, fields:DataTable) : string =
        let result = "CREATE TYPE " + typename + " AS ("
        let rowdef index =
            let record = fields.Rows.Item(index)
            "\n\t" + record.Item("fieldname").ToString() + " " + record.Item("fieldtypename").ToString() + " "
        let rec loop n acc =
            if n > 0 then
                loop (n - 1) (rowdef(n - 1) :: acc)
            else
                acc
        result + String.Join("", loop fields.Rows.Count []) + Environment.NewLine + ")"

    override this.GetDefColumns(name:string, objtype:ObjectType) : DataTable list =
        let parts = name.Split('.')
        let schema = parts.[0]
        let tblName = parts.[1]
        let script = String.Format("select column_name, is_nullable nullable, character_maximum_length as length, numeric_precision as precision, numeric_scale as scale, data_type as type_name from information_schema.columns where table_schema = '{0}' and table_name = '{1}' order by ordinal_position;", schema, tblName)
        let columns = this.CreateDataSet(script.ToString()).Tables.Item(0)
        [columns; null; null]

    override this.GetTextColumns(name:string, objtype:ObjectType) : string list =
        let parts = name.Split('.')
        let schema = parts.[0]
        let objName = parts.[1]
        let query =
            if objtype = ObjectType.TRIGGER then
                "select s.ACTION_STATEMENT from INFORMATION_SCHEMA.TRIGGERS s where s.TRIGGER_SCHEMA = '" + schema + "' and s.TRIGGER_NAME = '"
            elif objtype = ObjectType.VIEW then
                "select s.VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS s where  s.TABLE_SCHEMA = '" + schema + "' and s.TABLE_NAME = '"
            elif objtype = ObjectType.FUNCTION || objtype = ObjectType.PROCEDURE then
                "select s.ROUTINE_DEFINITION from INFORMATION_SCHEMA.ROUTINES s where  s.ROUTINE_SCHEMA = '" + schema + "' and s.ROUTINE_NAME = '"
            else ""
        let lst = this.CreateDataSet(query + objName + "'").Tables.Item(0)
        let rec loop n acc =
            if n > 0 then
                loop (n - 1) (lst.Rows.Item(n - 1).Item(0).ToString() :: acc)
            else
                acc
        loop lst.Rows.Count []

    override this.GetNameByTextColumns(searchStr:string, types:ObjectType list, caseSensitive:bool) : string =
        let rec loop n (acc:string list) =
            if n > 0 then
                let un = if String.Join("",acc).Length > 0 && n < types.Length then " union all " else ""
                if caseSensitive then
                    if types.Item(n - 1) = ObjectType.PROCEDURE then
                        loop (n - 1) (String.Format("select ROUTINE_SCHEMA || '.' || ROUTINE_NAME as name, ROUTINE_TYPE as type from INFORMATION_SCHEMA.ROUTINES where ROUTINE_CATALOG = '" + this._connection.Database + "' and ROUTINE_TYPE = 'PROCEDURE' and ROUTINE_DEFINITION like '%{0}%'" + un, searchStr) :: acc)
                    elif types.Item(n - 1) = ObjectType.TRIGGER then
                        loop (n - 1) (String.Format("select TRIGGER_SCHEMA || '.' || TRIGGER_NAME as name, 'TRIGGER' as type from INFORMATION_SCHEMA.TRIGGERS where TRIGGER_CATALOG = '" + this._connection.Database + "' and ACTION_STATEMENT like '%{0}%'" + un, searchStr) :: acc)
                    elif types.Item(n - 1) = ObjectType.VIEW then
                        loop (n - 1) (String.Format("select TABLE_SCHEMA || '.' || TABLE_NAME as name, 'VIEW' as type from INFORMATION_SCHEMA.VIEWS where TABLE_CATALOG = '" + this._connection.Database + "' and VIEW_DEFINITION like '%{0}%'" + un, searchStr) :: acc)
                    elif types.Item(n - 1) = ObjectType.FUNCTION then
                        loop (n - 1) (String.Format("select ROUTINE_SCHEMA || '.' || ROUTINE_NAME as name, ROUTINE_TYPE as type from INFORMATION_SCHEMA.ROUTINES where ROUTINE_CATALOG = '" + this._connection.Database + "' and ROUTINE_TYPE = 'FUNCTION' and ROUTINE_DEFINITION like '%{0}%'" + un, searchStr) :: acc)
                    else
                        loop (n - 1) acc
                else
                    if types.Item(n - 1) = ObjectType.PROCEDURE then
                        loop (n - 1) (String.Format("select ROUTINE_SCHEMA || '.' || ROUTINE_NAME as name, ROUTINE_TYPE as type from INFORMATION_SCHEMA.ROUTINES where ROUTINE_CATALOG = '" + this._connection.Database + "' and ROUTINE_TYPE = 'PROCEDURE' and lower(ROUTINE_DEFINITION) like '%{0}%'" + un, searchStr.ToLower()) :: acc)
                    elif types.Item(n - 1) = ObjectType.TRIGGER then
                        loop (n - 1) (String.Format("select TRIGGER_SCHEMA || '.' || TRIGGER_NAME as name, 'TRIGGER' as type from INFORMATION_SCHEMA.TRIGGERS where TRIGGER_CATALOG = '" + this._connection.Database + "' and lower(ACTION_STATEMENT) like '%{0}%'" + un, searchStr.ToLower()) :: acc)
                    elif types.Item(n - 1) = ObjectType.VIEW then
                        loop (n - 1) (String.Format("select TABLE_SCHEMA || '.' || TABLE_NAME as name, 'VIEW' as type from INFORMATION_SCHEMA.VIEWS where TABLE_CATALOG = '" + this._connection.Database + "' and lower(VIEW_DEFINITION) like '%{0}%'" + un, searchStr.ToLower()) :: acc)
                    elif types.Item(n - 1) = ObjectType.FUNCTION then
                        loop (n - 1) (String.Format("select ROUTINE_SCHEMA || '.' || ROUTINE_NAME as name, ROUTINE_TYPE as type from INFORMATION_SCHEMA.ROUTINES where ROUTINE_CATALOG = '" + this._connection.Database + "' and ROUTINE_TYPE = 'FUNCTION' and lower(ROUTINE_DEFINITION) like '%{0}%'" + un, searchStr.ToLower()) :: acc)
                    else
                        loop (n - 1) acc
            else acc
        String.Join("",loop types.Length [])

    override this.GenerateNameScript(search:string, types:ObjectType list, caseSensitive:bool) : string =
        let startQuery = "select name, type from ("
        let rec loop n (acc:string list) =
            if n > 0 then
                let un = if String.Join("",acc).Length <> startQuery.Length && n < types.Length then " union all " else ""
                if types.Item(n - 1) = ObjectType.PROCEDURE then
                    loop (n - 1) ("select ROUTINE_SCHEMA || '.' || ROUTINE_NAME as name, ROUTINE_TYPE as type from INFORMATION_SCHEMA.ROUTINES where ROUTINE_CATALOG = '" + this._connection.Database + "' and ROUTINE_TYPE = 'PROCEDURE'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.TRIGGER then
                    loop (n - 1) ("select TRIGGER_SCHEMA || '.' || TRIGGER_NAME as name, 'TRIGGER' as type from INFORMATION_SCHEMA.TRIGGERS where TRIGGER_CATALOG = '" + this._connection.Database + "'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.VIEW then
                    loop (n - 1) ("select TABLE_SCHEMA || '.' || TABLE_NAME as name, 'VIEW' as type from INFORMATION_SCHEMA.VIEWS where TABLE_CATALOG = '" + this._connection.Database + "'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.FUNCTION then
                    loop (n - 1) ("select ROUTINE_SCHEMA || '.' || ROUTINE_NAME as name, ROUTINE_TYPE as type from INFORMATION_SCHEMA.ROUTINES where ROUTINE_CATALOG = '" + this._connection.Database + "' and ROUTINE_TYPE = 'FUNCTION'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.TABLE then
                    loop (n - 1) ("select TABLE_SCHEMA || '.' || TABLE_NAME as name, TABLE_TYPE as type from INFORMATION_SCHEMA.TABLES where TABLE_CATALOG = '" + this._connection.Database + "' and TABLE_TYPE = 'BASE TABLE'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.FOREIGN_KEY then
                    loop (n - 1) ("select TABLE_SCHEMA || '.' || CONSTRAINT_NAME as name, CONSTRAINT_TYPE as type from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where TABLE_CATALOG = '" + this._connection.Database + "' and CONSTRAINT_TYPE = 'FOREIGN KEY'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.PRIMARY_KEY then
                    loop (n - 1) ("select TABLE_SCHEMA || '.' || CONSTRAINT_NAME as name, CONSTRAINT_TYPE as type from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where TABLE_CATALOG = '" + this._connection.Database + "' and CONSTRAINT_TYPE = 'PRIMARY KEY'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.INDEX then
                    loop (n - 1) ("select TABLE_SCHEMA || '.' || CONSTRAINT_NAME as name, CONSTRAINT_TYPE as type from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where TABLE_CATALOG = '" + this._connection.Database + "' and CONSTRAINT_TYPE = 'UNIQUE'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.SEQUENCE then
                    loop (n - 1) ("select SEQUENCE_SCHEMA || '.' || SEQUENCE_NAME as name, 'SEQUENCE' as type from INFORMATION_SCHEMA.SEQUENCES where SEQUENCE_CATALOG = '" + this._connection.Database + "'" + un :: acc)
                elif types.Item(n - 1) = ObjectType.TYPE then
                    loop (n - 1) ("select USER_DEFINED_TYPE_SCHEMA || '.' || USER_DEFINED_TYPE_NAME as name, 'TYPE' as type from INFORMATION_SCHEMA.USER_DEFINED_TYPES where USER_DEFINED_TYPE_CATALOG = '" + this._connection.Database + "'" + un :: acc)
                else
                    loop (n - 1) acc
            else
                acc
        let query = String.Join("",loop types.Length [])
        let cond =
            if caseSensitive then
                ") as tbl where name like '%" + search + "%' order by type, name;"
            else
                ") as tbl where lower(name) like '%" + search.ToLower() + "%' order by type, name;"
        startQuery + query + cond

    override this.GenerateSearchScript(search:string, caseSensitive:bool) : string =
        let query = "select distinct t.TABLE_SCHEMA || '.' || t.TABLE_NAME as name, TABLE_TYPE from INFORMATION_SCHEMA.TABLES as t inner join INFORMATION_SCHEMA.COLUMNS as c on t.TABLE_NAME = c.TABLE_NAME"
        let cond =
            if caseSensitive then
                " where t.TABLE_CATALOG = '" + this._connection.Database + "' and TABLE_TYPE = 'BASE TABLE' and COLUMN_NAME like '%" + search
            else
                " where t.TABLE_CATALOG = '" + this._connection.Database + "' and TABLE_TYPE = 'BASE TABLE' and lower(COLUMN_NAME) like '%" + search.ToLower()
        query + cond + "%' order by name"

    override this.GetForeignKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetPrimaryKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetIndexDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetSequenceDefinition(sequenceName:string) : string =
        let parts = sequenceName.Split('.')
        let schema = parts.[0]
        let name = parts.[1]
        let query = String.Format("select data_type,start_value,increment,minimum_value,maximum_value,cycle_option from INFORMATION_SCHEMA.SEQUENCES where SEQUENCE_SCHEMA = '{0}' and SEQUENCE_NAME='{1}'", schema, name)
        let row = this.CreateDataSet(query).Tables.Item(0).Rows.Item(0)
        let part1 = "CREATE SEQUENCE " + sequenceName + " INCREMENT " + row.Item("increment").ToString()
        let part2 = " MINVALUE " + row.Item("minimum_value").ToString() + " MAXVALUE " + row.Item("maximum_value").ToString() + " START " + row.Item("start_value").ToString()
        let part3 = if row.Item("cycle_option").ToString() = "YES" then " CYCLE" else " NO CYCLE"
        part1 + part2 + part3

    override this.GetSynonymDefinition(synonymName:string) : string =
        raise (NotImplementedException())

    override this.GetTypeDefinition(typeName:string) : string =
        let parts = typeName.Split('.')
        let schema = parts.[0]
        let name = parts.[1]
        let query = String.Format("SELECT 
        a.attname AS fieldname,
        at.typname AS fieldtypename,
        a.attnum AS fieldorder
FROM pg_catalog.pg_namespace ns
INNER JOIN pg_catalog.pg_type t ON ns.oid = t.typnamespace
INNER JOIN pg_class c ON c.oid = t.typrelid
INNER JOIN pg_catalog.pg_attribute a ON a.attrelid = c.oid
INNER JOIN pg_catalog.pg_type at ON a.atttypid = at.oid
WHERE ns.nspname = '{0}' AND t.typname = '{1}' ORDER BY fieldorder", schema, name)
        this.ComposeTableType(typeName, this.CreateDataSet(query).Tables.Item(0))

    override this.GetJobDefinition(jobName:string) : string =
        raise (NotImplementedException())

    override this.GetReportDefinition(reportName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedProcedureDefinition(procName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedTriggerDefinition(triggerName:string) : string =
        raise (NotImplementedException())
