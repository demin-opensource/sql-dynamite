﻿namespace SqlDynamite.Common

open System
open System.Data
open System.Text
open ServiceStack.Redis

type RedisMetadataFinder() =
    inherit MetadataFinder()

    override this.GetDatabaseList (scname:string) : string list =
        let byteArrayToString (byteArray : byte array) =
            let sb = new System.Text.StringBuilder()
            for b in byteArray do
                ignore(sb.Append(Convert.ToInt32(b) - 48))
            sb.ToString()
        let parts = this._connection.ConnectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        let config = client.ConfigGet("databases")
        let dbcount = Int32.Parse(byteArrayToString(config.[1]))
        let rec loop n acc =
            if n > 0 then
                let m = n - 1
                loop m ("db" + m.ToString() :: acc)
            else
                acc
        loop dbcount []

    override this.Compose(tablename:string, fields:DataTable, keys:DataTable, checks:DataTable) : string =
        raise (NotImplementedException())

    override this.GetDefColumns(name:string, objtype:ObjectType) : DataTable list =
        raise (NotImplementedException())

    override this.GetNameByTextColumns(searchStr:string, types:ObjectType list, caseSensitive:bool) : string =
        searchStr

    override this.GetTextColumns(name:string, objtype:ObjectType) : string list =
        raise (NotImplementedException())

    override this.GenerateNameScript(search:string, types:ObjectType list, caseSensitive:bool) : string =
        search

    override this.GenerateSearchScript(search:string, caseSensitive:bool) : string =
        search

    override this.GetForeignKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetPrimaryKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetIndexDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetSequenceDefinition(sequenceName:string) : string =
        raise (NotImplementedException())

    override this.GetSynonymDefinition(synonymName:string) : string =
        raise (NotImplementedException())

    override this.GetTypeDefinition(typeName:string) : string =
        raise (NotImplementedException())

    override this.GetJobDefinition(jobName:string) : string =
        raise (NotImplementedException())

    override this.GetReportDefinition(reportName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedProcedureDefinition(procName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedTriggerDefinition(triggerName:string) : string =
        raise (NotImplementedException())

    member this.GetStringValue(keyName:string) : string =
        let parts = this._connection.ConnectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        client.GetValue(keyName)

    member this.GetListValue(keyName:string) : string =
        let parts = this._connection.ConnectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        let lst = client.GetAllItemsFromList(keyName)
        String.Join(Environment.NewLine, lst)

    member this.GetHashValue(keyName:string) : string =
        let parts = this._connection.ConnectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        let lst = client.GetAllEntriesFromHash(keyName)
        String.Join(Environment.NewLine, lst)

    member this.GetSetValue(keyName:string) : string =
        let parts = this._connection.ConnectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        let lst = client.GetAllItemsFromSet(keyName)
        String.Join(Environment.NewLine, lst)

    member this.GetSortedSetValue(keyName:string) : string =
        let parts = this._connection.ConnectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        let lst = client.GetAllItemsFromSortedSet(keyName)
        String.Join(Environment.NewLine, lst)

    member this.GetStreamValue(keyName:string) : string =
        let parts = this._connection.ConnectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        let rt = client.ExecLua("return redis.call('XRANGE', '" + keyName + "', '-', '+')")
        let rec loop (rt:RedisText, l, sb:StringBuilder) =
            if rt.Children <> null then
                for rtc in rt.Children do
                    if rtc.Children <> null then
                        loop (rtc, l + 1, sb)
                    else
                        let _ = sb.Append(String.replicate l "\t" + rtc.Text + Environment.NewLine)
                        ()
        let sb = new StringBuilder()
        loop (rt, -1, sb)
        sb.ToString()
