﻿namespace SqlDynamite.Common

open System
open Neo4j.Driver.V1

type Neo4jConnection() =
    inherit NoSqlConnection()

    override this.Open () =
        let parts = this.ConnectionString.Split(';')
        let url = parts.[0]
        let username = if parts.Length = 3 then parts.[1] else null
        let password = if parts.Length = 3 then parts.[2] else null
        let authToken = if (String.IsNullOrWhiteSpace(password) || String.IsNullOrWhiteSpace(username)) then AuthTokens.None else AuthTokens.Basic(username, password)

        let driver = GraphDatabase.Driver(url, authToken)
        let _ = driver.Session().Run("Match () Return 1 Limit 1")
        ()
