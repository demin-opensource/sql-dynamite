﻿namespace SqlDynamite.Common

open System
open Vibrant.InfluxDB.Client

type InfluxConnection() =
    inherit NoSqlConnection()

    override this.Open () =
        let parts = this.ConnectionString.Split(';')
        let url = parts.[0]
        let username = if parts.Length > 2 then parts.[1] else null
        let password = if parts.Length > 2 then parts.[2] else null
        let databaseName = if parts.Length = 2 then parts.[1] elif parts.Length = 4 then parts.[3] else null
        if not (String.IsNullOrWhiteSpace(databaseName)) then this.ChangeDatabase(databaseName)
        
        let client = if parts.Length > 2 then new InfluxClient(new Uri(url), username, password) else new InfluxClient(new Uri(url))
        let ping =
            async {
                let! _ = client.PingAsync() |> Async.AwaitTask
                ()
            }
        ping |> Async.RunSynchronously
        ()
