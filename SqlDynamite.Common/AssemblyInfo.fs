﻿namespace ConsoleApplication1.AssemblyInfo

open System.Reflection
open System.Runtime.InteropServices

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[<assembly: AssemblyTitle("SqlDynamite.Common")>]
[<assembly: AssemblyDescription("")>]
[<assembly: AssemblyConfiguration("")>]
[<assembly: AssemblyCompany("Fire Lizard Software")>]
[<assembly: AssemblyProduct("SQL Dynamite")>]
[<assembly: AssemblyCopyright("Copyright © Anatoliy Sova 2005 - 2020")>]
[<assembly: AssemblyTrademark("")>]
[<assembly: AssemblyCulture("")>]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[<assembly: ComVisible(false)>]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[<assembly: Guid("35269b96-e0fc-4fb5-8abc-b6e311a26367")>]

// Version information for an assembly consists of the following four values:
//
//       Major Version
//       Minor Version
//       Build Number
//       Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [<assembly: AssemblyVersion("1.0.*")>]
[<assembly: AssemblyVersion("2.5.1.4")>]
[<assembly: AssemblyFileVersion("2.5.1.4")>]

do
    ()