﻿namespace SqlDynamite.Common

open System
open System.Data
open System.Data.Common

[<AbstractClass>]
type NoSqlConnection() =
    inherit DbConnection()

    [<DefaultValue(false)>]
    val mutable _connectionString:string

    [<DefaultValue(false)>]
    val mutable private _database:string

    override this.Close() =
        raise (NotImplementedException())

    override this.CreateDbCommand() : DbCommand =
        raise (NotImplementedException())

    override this.ChangeDatabase(databaseName:string) =
        this._database <- databaseName

    override this.BeginDbTransaction(isolationLevel:IsolationLevel) : DbTransaction =
        raise (NotImplementedException())

    override this.State with get() = raise (NotImplementedException())

    override this.ServerVersion with get() = raise (NotImplementedException())

    override this.DataSource with get() = raise (NotImplementedException())

    override this.Database with get() = this._database

    override this.ConnectionString with get() = this._connectionString and set(v) = this._connectionString <- v
