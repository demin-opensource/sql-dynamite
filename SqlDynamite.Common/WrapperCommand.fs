﻿namespace SqlDynamite.Common

open System.Data
open System.Data.Common

type WrapperCommand(innerCommand:IDbCommand) =
    inherit DbCommand()

    member this.InnerCommand = innerCommand

    override this.CommandText with get() = this.InnerCommand.CommandText and set(v) = this.InnerCommand.CommandText <- v

    override this.CommandTimeout with get() = this.InnerCommand.CommandTimeout and set(v) = this.InnerCommand.CommandTimeout <- v

    override this.CommandType with get() = this.InnerCommand.CommandType and set(v) = this.InnerCommand.CommandType <- v

    override this.DbConnection with get() = this.InnerCommand.GetType().GetProperty("ConnectionString").GetGetMethod().Invoke(this.InnerCommand, null) :?> DbConnection
                                    and set(v) = this.InnerCommand.GetType().GetProperty("ConnectionString").GetSetMethod().Invoke(this.InnerCommand, [|v|]) :?> unit

    override this.DbParameterCollection with get() = this.InnerCommand.GetType().GetProperty("DbParameterCollection").GetGetMethod().Invoke(this.InnerCommand, null) :?> DbParameterCollection

    override this.DbTransaction with get() = this.InnerCommand.GetType().GetProperty("DbTransaction").GetGetMethod().Invoke(this.InnerCommand, null) :?> DbTransaction
                                and set(v) = this.InnerCommand.GetType().GetProperty("DbTransaction").GetSetMethod().Invoke(this.InnerCommand, [|v|]) :?> unit

    override this.DesignTimeVisible with get() = this.InnerCommand.GetType().GetProperty("DesignTimeVisible").GetGetMethod().Invoke(this.InnerCommand, null) :?> bool
                                    and set(v) = this.InnerCommand.GetType().GetProperty("DesignTimeVisible").GetSetMethod().Invoke(this.InnerCommand, [|v|]) :?> unit

    override this.UpdatedRowSource with get() = this.InnerCommand.GetType().GetProperty("UpdatedRowSource").GetGetMethod().Invoke(this.InnerCommand, null) :?> UpdateRowSource
                                    and set(v) = this.InnerCommand.GetType().GetProperty("UpdatedRowSource").GetSetMethod().Invoke(this.InnerCommand, [|v|]) :?> unit

    override this.Cancel() : unit =
        this.InnerCommand.GetType().GetMethod("Cancel").Invoke(this.InnerCommand, null) :?> unit

    override this.ExecuteNonQuery() : int =
        this.InnerCommand.GetType().GetMethod("ExecuteNonQuery").Invoke(this.InnerCommand, null) :?> int

    override this.ExecuteScalar() : obj =
        this.InnerCommand.GetType().GetMethod("ExecuteScalar").Invoke(this.InnerCommand, null)

    override this.Prepare() : unit =
        this.InnerCommand.GetType().GetMethod("Prepare").Invoke(this.InnerCommand, null) :?> unit

    override this.CreateDbParameter() : DbParameter =
        this.InnerCommand.GetType().GetMethod("CreateDbParameter").Invoke(this.InnerCommand, null) :?> DbParameter

    override this.ExecuteDbDataReader(behavior:CommandBehavior) : DbDataReader =
        this.InnerCommand.GetType().GetMethod("ExecuteDbDataReader").Invoke(this.InnerCommand, null) :?> DbDataReader
