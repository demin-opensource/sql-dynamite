﻿namespace SqlDynamite.Common

open System
open ServiceStack.Redis

type RedisConnection() =
    inherit NoSqlConnection()

    override this.Open () =
        let parts = this._connectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.Db <- int64(db)
        ignore(client.GetClientsInfo())
        this.ChangeDatabase(sprintf "%d" client.Db)
        ()

    override this.ServerVersion with get() =
        let parts = this._connectionString.Split(';')
        let cs = parts.[0].Split(':')
        let db = if String.IsNullOrWhiteSpace(parts.[1]) then 0 else Int32.Parse(parts.[1].Replace("db", ""))
        let port = if cs.Length = 2 then Int32.Parse(cs.[1]) else 6379
        let password = if parts.Length = 3 then parts.[2].Replace("Password=", "") else ""
        let client =
            if String.IsNullOrWhiteSpace(password) then new RedisClient(cs.[0], port)
            else new RedisClient(cs.[0], port, password, int64(0))
        client.ServerVersion
