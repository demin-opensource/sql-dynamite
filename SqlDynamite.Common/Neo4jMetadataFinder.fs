﻿namespace SqlDynamite.Common

open System
open System.Data
open Neo4j.Driver.V1

type Neo4jMetadataFinder() =
    inherit MetadataFinder()

    override this.Compose(tablename:string, fields:DataTable, keys:DataTable, checks:DataTable) : string =
        raise (NotImplementedException())

    override this.GetDefColumns(name:string, objtype:ObjectType) : DataTable list =
        [null; null; null]

    override this.GetTextColumns(name:string, objtype:ObjectType) : string list =
        let parts = this._connection.ConnectionString.Split(';')
        let url = parts.[0]
        let username = parts.[1]
        let password = parts.[2]
        let authToken = if (String.IsNullOrWhiteSpace(password) || String.IsNullOrWhiteSpace(username)) then AuthTokens.None else AuthTokens.Basic(username, password)

        let driver = GraphDatabase.Driver(url, authToken)
        let constraints = driver.Session().Run(String.Format("CALL db.constraints()
        YIELD description AS desc
        WITH desc
        WHERE desc CONTAINS ':{0}'
        RETURN desc", name))
        [for cnt in constraints do yield cnt.Item("desc").ToString()]

    override this.GetNameByTextColumns(searchStr:string, types:ObjectType list, caseSensitive:bool) : string =
        searchStr

    override this.GenerateNameScript(search:string, types:ObjectType list, caseSensitive:bool) : string =
        search

    override this.GenerateSearchScript(search:string, caseSensitive:bool) : string =
        search

    override this.GetForeignKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetPrimaryKeyDefinition(constraintName:string) : string =
        raise (NotImplementedException())

    override this.GetIndexDefinition(constraintName:string) : string =
        let parts = this._connection.ConnectionString.Split(';')
        let url = parts.[0]
        let username = parts.[1]
        let password = parts.[2]
        let authToken = if (String.IsNullOrWhiteSpace(password) || String.IsNullOrWhiteSpace(username)) then AuthTokens.None else AuthTokens.Basic(username, password)

        let driver = GraphDatabase.Driver(url, authToken)
        let indexes = driver.Session().Run(String.Format("call db.indexes()
        YIELD indexName AS name, description AS desc
        WITH name, desc
        WHERE name = '{0}'
        RETURN desc", constraintName))
        String.Join(Environment.NewLine, [for idx in indexes do yield idx.Item("desc")])

    override this.GetSequenceDefinition(sequenceName:string) : string =
        raise (NotImplementedException())

    override this.GetSynonymDefinition(synonymName:string) : string =
        raise (NotImplementedException())

    override this.GetTypeDefinition(typeName:string) : string =
        raise (NotImplementedException())

    override this.GetJobDefinition(jobName:string) : string =
        raise (NotImplementedException())

    override this.GetReportDefinition(reportName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedProcedureDefinition(procName:string) : string =
        raise (NotImplementedException())

    override this.GetExtendedTriggerDefinition(triggerName:string) : string =
        raise (NotImplementedException())
