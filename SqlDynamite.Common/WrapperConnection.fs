﻿namespace SqlDynamite.Common

open System
open System.Data
open System.Data.Common

type WrapperConnection(innerConnection:IDbConnection) =
    inherit DbConnection()

    member this.InnerConnection = innerConnection

    override this.Open() =
        this.InnerConnection.Open()

    override this.Close() =
        this.InnerConnection.Close()

    override this.CreateDbCommand() : DbCommand =
        raise (NotImplementedException())

    member this.CreateCommand() : IDbCommand =
        this.InnerConnection.GetType().GetMethod("CreateCommand").Invoke(this.InnerConnection, null) :?> IDbCommand

    override this.BeginDbTransaction(isolationLevel:IsolationLevel) : DbTransaction =
        raise (NotImplementedException())

    member this.BeginTransaction(isolationLevel:IsolationLevel) : IDbTransaction =
        this.InnerConnection.GetType().GetMethod("BeginTransaction").Invoke(this.InnerConnection, [|isolationLevel|]) :?> IDbTransaction

    override this.ChangeDatabase(databaseName:string) =
        this.InnerConnection.ChangeDatabase(databaseName)

    override this.State with get() =
                             this.InnerConnection.State

    override this.ServerVersion with get() =
                                     this.InnerConnection.GetType().GetProperty("ServerVersion").GetGetMethod().Invoke(this.InnerConnection, null) :?> string

    override this.DataSource with get() =
                                  this.InnerConnection.GetType().GetProperty("DataSource").GetGetMethod().Invoke(this.InnerConnection, null) :?> string

    override this.Database with get() =
                                this.InnerConnection.Database

    override this.ConnectionString with get() = this.InnerConnection.GetType().GetProperty("ConnectionString").GetGetMethod().Invoke(this.InnerConnection, null) :?> string
                                   and set(v) = this.InnerConnection.GetType().GetProperty("ConnectionString").GetSetMethod().Invoke(this.InnerConnection, [|v|]) :?> unit
